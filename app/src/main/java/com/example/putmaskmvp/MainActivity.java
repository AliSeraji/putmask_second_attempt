package com.example.putmaskmvp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.putmaskmvp.Adopters.FontsAdapter;
import com.example.putmaskmvp.Utils.FontProvider;
import com.example.putmaskmvp.videoTrimmer.utils.FileUtils;
import com.example.putmaskmvp.videoTrimmer.view.ThumbnailRecyclerAdopter;
import com.example.putmaskmvp.videoTrimmer.view.TimeLineView;
import com.example.putmaskmvp.viewmodel.Font;
import com.example.putmaskmvp.viewmodel.Layer;
import com.example.putmaskmvp.viewmodel.TextLayer;
import com.example.putmaskmvp.widget.Bar;
import com.example.putmaskmvp.widget.MotionView;
import com.example.putmaskmvp.widget.entity.ImageEntity;
import com.example.putmaskmvp.widget.entity.MotionEntity;
import com.example.putmaskmvp.widget.entity.TextEntity;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * in order to start EDIT WHAT WE SAVED you have to use  editleft and editRight set them visible and then do what you want I have set the listeners
 */

public class MainActivity extends AppCompatActivity implements TextEditorDialogFragment.OnTextLayerCallback,SurfaceHolder.Callback {


    public static final int SELECT_STICKER_REQUEST_CODE = 123;
    private int SELECT_VIDEO_REQUEST_CODE = 10;
    private int SELECT_IMAGE_REQUEST_CODE = 11;
    private String videoPath;
    private Uri videoUri, imageUri;
    protected MotionView motionView;
    protected View textEntityEditPanel;
    static final String EXTRA_VIDEO_PATH = "EXTRA_VIDEO_PATH";
    static final String VIDEO_TOTAL_DURATION = "VIDEO_TOTAL_DURATION";
    private static final int REQUEST_VIDEO_TRIMMER = 0x01;
    private static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    private static final String GET_VIDEO_URI_FROM_HGL = "get_video_uri";
    private static final String GET_VIDEO_DURATION_FROM_HGL = "get_video_duration_from_hgl";
    private long videoDuration = 0;
    /////////////////////////////////////
    private int defpos = -1;
    private SurfaceView surfaceView;
    private MediaPlayer mediaPlayer;
    private SurfaceHolder surfaceHolder;
    private ThumbnailRecyclerAdopter thumbnailRecyclerAdopter;
    private RecyclerView recyclerView;
    private MediaMetadataRetriever mediaMetadataRetriever;
    @BindView(R.id.video_timer) TextView elapsedTime;
    @BindView(R.id.main_timeLineView) TimeLineView mTimeLineView;
    @BindView(R.id.main_handlerTop) SeekBar mHolderTopView;
    @BindView(R.id.time_line_container)RelativeLayout mTimeLineContainer;
    @BindView(R.id.play_video) ImageButton playBtn;
    @BindView(R.id.pause_video)ImageButton pauseBtn;
    @BindView(R.id.record_video)ImageButton recVideo;
    @BindView(R.id.stop_record_video)ImageButton stopRecVideo;
    @BindView(R.id.choose_video)Button chooseBtn;
    @BindView(R.id.mute_btn)Button muteBtn;
    @BindView(R.id.un_mute_btn)Button unMuteBtn;
    @BindView(R.id.next_frame)Button nextFrameBtn;
    @BindView(R.id.prev_frame)Button prevFrameBtn;
    @BindView(R.id.goto_record_video)Button gotoRecBtn;
    @BindView(R.id.main_left)View mainLeftView;
    @BindView(R.id.main_right)View mainRightView;
    @BindView(R.id.rec_mode)View recMode;
    @BindView(R.id.face_track_left)View faceTrackLeft;
    @BindView(R.id.face_track_right)View faceTrackRight;
    @BindView(R.id.start_faceTrack)Button startFaceTrackBtn;
    @BindView(R.id.edit_left)View editleft;
    @BindView(R.id.edit_right)View editRight;
    //@BindView(R.id.set_def_pos)Button defPos;
    @BindView(R.id.add_image)Button imBtn;
    @BindView(R.id.add_sticker)Button stBtn;
    @BindView(R.id.add_text)Button txtBtn;
    //@BindView(R.id.del)Button delBtn;
    //@BindView(R.id.record_video)Button recBtn;
    //@BindView(R.id.stop_recoding_video)Button sRecBtn;
    @BindView(R.id.main_linear) LinearLayout linear;
    @BindView(R.id.scroll_bars) LinearLayout scrollView;
    private ArrayList<Bar> bars=new ArrayList<Bar>();
    private float leftPos;
    private boolean storageAccess=false;
    ////////////////////////////////////
    private final MotionView.MotionViewCallback motionViewCallback = new MotionView.MotionViewCallback() {
        @Override
        public void onEntitySelected(@Nullable MotionEntity entity) {
            if (entity instanceof TextEntity) {
                //textEntityEditPanel.setVisibility(View.VISIBLE);
            } else {
                //textEntityEditPanel.setVisibility(View.GONE);
            }
        }

        @Override
        public void onEntityDoubleTap(@NonNull MotionEntity entity) {
            startTextEntityEditing();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Intent exIntent = getIntent();
        if (exIntent != null) {
            videoUri = exIntent.getParcelableExtra(GET_VIDEO_URI_FROM_HGL);
            videoDuration = exIntent.getLongExtra(GET_VIDEO_DURATION_FROM_HGL, 10L);
        }

        this.fontProvider = new FontProvider(getResources());
        motionView = findViewById(R.id.main_motion_view);
        textEntityEditPanel = findViewById(R.id.main_motion_text_entity_edit_panel);
        motionView.setMotionViewCallback(motionViewCallback);
        addSticker(R.drawable.pikachu_2);

        initTextEntitiesListeners();
        ///////////////////////////////
        initializeSurfaceView();
       if (videoUri != null && videoDuration > 0) {
           //setUpMargins();
           mTimeLineView.setVideo(videoUri);
           setUpTimeLinearContainer();
       }

    }

    private FontProvider fontProvider;


    private void initializeSurfaceView() {
        surfaceView = findViewById(R.id.video_player);
        mediaPlayer = new MediaPlayer();
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceView.setKeepScreenOn(true);
        mediaPlayer.setOnCompletionListener(
                new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mediaPlayer.seekTo(0);
                        mHolderTopView.setProgress(0);
                        pauseMode();
                    }

                });
    }

    private void addSticker(final int stickerResId) {
        motionView.post(new Runnable() {
            @Override
            public void run() {
                Layer layer = new Layer();
                Bitmap pica = BitmapFactory.decodeResource(getResources(), stickerResId);
                ImageEntity entity = new ImageEntity(layer, pica, motionView.getWidth(), motionView.getHeight());
                motionView.addEntityAndPosition(entity);
            }
        });
    }

    private void addImage(final Bitmap pic) {
        motionView.post(new Runnable() {
            @Override
            public void run() {
                //BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                Layer layer = new Layer();
                ImageEntity entity = new ImageEntity(layer, pic, motionView.getWidth(), motionView.getHeight());
                motionView.addEntityAndPosition(entity);
            }
        });
    }


    private void initTextEntitiesListeners() {
        findViewById(R.id.text_entity_font_size_increase).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                increaseTextEntitySize();
            }
        });
        findViewById(R.id.text_entity_font_size_decrease).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                decreaseTextEntitySize();
            }
        });
        findViewById(R.id.text_entity_color_change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeTextEntityColor();
            }
        });
        findViewById(R.id.text_entity_font_change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeTextEntityFont();
            }
        });
        findViewById(R.id.text_entity_edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startTextEntityEditing();
            }
        });
    }

    private void increaseTextEntitySize() {
        TextEntity textEntity = currentTextEntity();
        if (textEntity != null) {
            textEntity.getLayer().getFont().increaseSize(TextLayer.Limits.FONT_SIZE_STEP);
            textEntity.updateEntity();
            motionView.invalidate();
        }
    }

    private void decreaseTextEntitySize() {
        TextEntity textEntity = currentTextEntity();
        if (textEntity != null) {
            textEntity.getLayer().getFont().decreaseSize(TextLayer.Limits.FONT_SIZE_STEP);
            textEntity.updateEntity();
            motionView.invalidate();
        }
    }

    private void changeTextEntityColor() {
        TextEntity textEntity = currentTextEntity();
        if (textEntity == null) {
            return;
        }

        int initialColor = textEntity.getLayer().getFont().getColor();

        ColorPickerDialogBuilder
                .with(MainActivity.this)
                .setTitle(R.string.select_color)
                .initialColor(initialColor)
                .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                .density(8) // magic number
                .setPositiveButton(R.string.Ok, new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        TextEntity textEntity = currentTextEntity();
                        if (textEntity != null) {
                            textEntity.getLayer().getFont().setColor(selectedColor);
                            textEntity.updateEntity();
                            motionView.invalidate();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .build()
                .show();
    }

    private void changeTextEntityFont() {
        final List<String> fonts = fontProvider.getFontNames();
        FontsAdapter fontsAdapter = new FontsAdapter(this, fonts, fontProvider);
        new AlertDialog.Builder(this)
                .setTitle(R.string.select_font)
                .setAdapter(fontsAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        TextEntity textEntity = currentTextEntity();
                        if (textEntity != null) {
                            textEntity.getLayer().getFont().setTypeface(fonts.get(which));
                            textEntity.updateEntity();
                            motionView.invalidate();
                        }
                    }
                })
                .show();
    }

    private void startTextEntityEditing() {
        TextEntity textEntity = currentTextEntity();
        if (textEntity != null) {
            TextEditorDialogFragment fragment = TextEditorDialogFragment.getInstance(textEntity.getLayer().getText());
            fragment.show(getFragmentManager(), TextEditorDialogFragment.class.getName());
        }
    }

    @Nullable
    private TextEntity currentTextEntity() {
        if (motionView != null && motionView.getSelectedEntity() instanceof TextEntity) {
            return ((TextEntity) motionView.getSelectedEntity());
        } else {
            return null;
        }
    }


    protected void addTextSticker() {
        TextLayer textLayer = createTextLayer();
        TextEntity textEntity = new TextEntity(textLayer, motionView.getWidth(),
                motionView.getHeight(), fontProvider);
        motionView.addEntityAndPosition(textEntity);

        // move text sticker up so that its not hidden under keyboard
        PointF center = textEntity.absoluteCenter();
        center.y = center.y * 0.5F;
        textEntity.moveCenterTo(center);

        // redraw
        motionView.invalidate();

        startTextEntityEditing();
    }

    private TextLayer createTextLayer() {
        TextLayer textLayer = new TextLayer();
        Font font = new Font();

        font.setColor(TextLayer.Limits.INITIAL_FONT_COLOR);
        font.setSize(TextLayer.Limits.INITIAL_FONT_SIZE);
        font.setTypeface(fontProvider.getDefaultFontName());

        textLayer.setFont(font);

        if (BuildConfig.DEBUG) {
            textLayer.setText("Hello, world :))");
        }

        return textLayer;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_STICKER_REQUEST_CODE) {
                if (data != null) {
                    int stickerId = data.getIntExtra(StickerSelectActivity.EXTRA_STICKER_ID, 0);
                    if (stickerId != 0) {
                        addSticker(stickerId);
                    }
                }
            }
            if (requestCode == SELECT_VIDEO_REQUEST_CODE) {
                if (data != null) {
                    videoUri = data.getData();
                    if (videoUri != null) {
                        startTrimActivity(videoUri);
                    }
                }
            }
            if (requestCode == SELECT_IMAGE_REQUEST_CODE) {
                if (data != null) {
                    imageUri = data.getData();
                    Glide.with(getApplicationContext())
                            .asBitmap()
                            .apply(new RequestOptions().override(500, 300))
                            .load(imageUri)
                            .listener(new RequestListener<Bitmap>() {
                                          @Override
                                          public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Bitmap> target, boolean b) {
                                              return false;
                                          }
                                          @Override
                                          public boolean onResourceReady(Bitmap bitmap, Object o, Target<Bitmap> target, DataSource dataSource, boolean b) {
                                              addImage(bitmap);
                                              return false;
                                          }
                                      }
                            ).submit();
                }
            }
        }
    }

    @Override
    public void textChanged(@NonNull String text) {
        TextEntity textEntity = currentTextEntity();
        if (textEntity != null) {
            TextLayer textLayer = textEntity.getLayer();
            if (!text.equals(textLayer.getText())) {
                textLayer.setText(text);
                textEntity.updateEntity();
                motionView.invalidate();
            }
        }
    }

    @OnClick(R.id.choose_video)
    public void chooseVideo() {
        requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, getString(R.string.permission_read_storage_rationale), REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        videoUri = null;
    }

    @OnClick(R.id.play_video)
    public void playVideo(View view) {
        if (!mediaPlayer.isPlaying() && videoUri!=null) {
            mediaPlayer.seekTo(mediaPlayer.getCurrentPosition());
            mediaPlayer.start();
            playBtn.setVisibility(View.GONE);
            pauseBtn.setVisibility(View.VISIBLE);
            //playMode();
        }
    }

  /* @OnClick(R.id.record_video)
    public void recordVideo(View view){
        //sRecBtn.setBackground(getDrawable(R.drawable.button_effect_record));
        //recBtn.setBackground(getDrawable(R.drawable.button_effect_record));
    }
    /*
    @OnClick(R.id.stop_recoding_video)
    public void stopRecording(View view){
        //sRecBtn.setBackground(getDrawable(R.drawable.button_effect));
        //recBtn.setBackground(getDrawable(R.drawable.button_effect));

    }*/

    @OnClick(R.id.pause_video)
    public void pauseVideo(View view){
            mediaPlayer.pause();
            pauseBtn.setVisibility(View.GONE);
            playBtn.setVisibility(View.VISIBLE);
            //pauseMode();
    }

    private void pauseMode(){
        pauseBtn.setVisibility(View.GONE);
        playBtn.setVisibility(View.VISIBLE);
        chooseBtn.setVisibility(View.VISIBLE);
        imBtn.setVisibility(View.VISIBLE);
        stBtn.setVisibility(View.VISIBLE);
        txtBtn.setVisibility(View.VISIBLE);
        //delBtn.setVisibility(View.VISIBLE);
        //defPos.setVisibility(View.VISIBLE);
    }
    private void playMode(){
        pauseBtn.setVisibility(View.VISIBLE);
        //sRecBtn.setVisibility(View.VISIBLE);
        //recBtn.setVisibility(View.VISIBLE);
        playBtn.setVisibility(View.GONE);
        chooseBtn.setVisibility(View.GONE);
        imBtn.setVisibility(View.GONE);
        stBtn.setVisibility(View.GONE);
        txtBtn.setVisibility(View.GONE);
        //delBtn.setVisibility(View.GONE);
        //defPos.setVisibility(View.GONE);
    }

    public String getPath(@NonNull Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        getContentResolver();
        Cursor cursor = getApplicationContext().getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        mediaPlayer.setDisplay(holder);
        if (videoUri != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.reset();
                try {
                    mediaPlayer.setDataSource(this.getApplicationContext(), videoUri);
                    mediaPlayer.prepare();
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);


                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    mediaPlayer.setDataSource(this.getApplicationContext(), videoUri);
                    mediaPlayer.prepare();
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mediaPlayer.release();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @OnClick(R.id.add_sticker)
    public void addStickerButton() {
        Intent intent = new Intent(this, StickerSelectActivity.class);
        startActivityForResult(intent, SELECT_STICKER_REQUEST_CODE);
    }

    @OnClick(R.id.add_text)
    public void addTextButton() {
        addTextSticker();
    }

    @OnClick(R.id.add_image)
    public void addImageButton() {
        imageUri = null;
        Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickIntent.createChooser(pickIntent, "select image"), SELECT_IMAGE_REQUEST_CODE);
    }
/*
    @OnClick(R.id.del)
    public void deleteEntity() {
        motionView.deletedSelectedEntity();
    }
*/
    private void startTrimActivity(@NonNull Uri uri) {
        Intent intent = new Intent(this, TrimmerActivity.class);
        intent.putExtra(EXTRA_VIDEO_PATH, FileUtils.getPath(getApplicationContext(), uri));
        intent.putExtra(VIDEO_TOTAL_DURATION, getMediaDuration(uri));
        startActivity(intent);
    }

    private int getMediaDuration(Uri uriOfFile) {
        MediaPlayer mp = MediaPlayer.create(this, uriOfFile);
        int duration = mp.getDuration();
        return duration;
    }

    private void pickFromGallery() {
        Intent intent = new Intent();
        intent.setTypeAndNormalize("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_video)), SELECT_VIDEO_REQUEST_CODE);
    }

    private void requestPermission(final String permission, String rationale, final int requestCode) {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getString(R.string.permission_title_rationale));
                builder.setMessage(rationale);
                builder.setPositiveButton(getString(R.string.Ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);
                    }
                });
                builder.setNegativeButton(getString(R.string.cancel), null);
                builder.show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
            }
        }
        else {
            pickFromGallery();
            videoUri = null;
        }
    }

    @SuppressLint("SetTextI18n")
    private void showElapsedTime(int timeT){
        int sec=timeT/1000;
        int min=timeT/60000;
        int hour=timeT/3600000;
        min-=60*hour;
        sec-=(60*min)+(3600*hour);
        elapsedTime.setText(hour+":"+min+":"+sec);
    }
 /*   @OnClick(R.id.set_def_pos)
    public void setDefPos() {
        defpos++;
        if (defpos <= 2)
            motionView.setDefPos(defpos);
        else {
            defpos = 0;
            motionView.setDefPos(defpos);
        }

    }
*/
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void setUpMargins() {
        int marge=mTimeLineContainer.getWidth();
        int widthSeek = mHolderTopView.getThumb().getMinimumWidth() / 2;

        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mHolderTopView.getLayoutParams();
        lp.setMargins(marge - widthSeek, 0, marge - widthSeek, 0);
        mHolderTopView.setLayoutParams(lp);

        lp = (RelativeLayout.LayoutParams) mTimeLineView.getLayoutParams();
        lp.setMargins(marge, 0, marge, 0);
        mTimeLineView.setLayoutParams(lp);
    }
    private void setUpTimeLinearContainer(){
        final Handler mHandler = new Handler();
        mHolderTopView.setMax((int)videoDuration);


        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    int mCurrentPosition = mediaPlayer.getCurrentPosition();
                    mHolderTopView.setProgress(mCurrentPosition);

                    showElapsedTime(mCurrentPosition);
                }
                mHandler.postDelayed(this, 1000);
            }
        });

        SeekBar.OnSeekBarChangeListener timelineBarSeekBarChangeListener
                = new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser){
                    if(mediaPlayer != null){
                        mediaPlayer.seekTo(progress);
                        showElapsedTime(progress);
                        //PointF f=getSeekBarPos(mHolderTopView,linear,mTimeLineContainer);
                    }
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        };
        mHolderTopView.setOnSeekBarChangeListener(timelineBarSeekBarChangeListener);

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    pickFromGallery();
                    videoUri = null;
                } else {
                    return;
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

        }
    }
    private PointF getSeekBarPos(SeekBar seekBar,LinearLayout linear,RelativeLayout relative1){
        int x=seekBar.getProgress();
        int max=seekBar.getMax();
        float rate=(float)x/max;
        float rateX=rate*seekBar.getWidth();
        float xStart=linear.getX();
        rateX+=xStart;
        //Log.i("getSeekbarPos","pose of seekbar"+rateX+" rate:"+rate+" width:"+seekBar.getWidth()+" X:"+linear.getX());
        float r=relative1.getY()+seekBar.getHeight();
        //Log.i("getSeekbarPos","pose of seekbar"+rateX+" rate:"+rate+" width:"+seekBar.getWidth()+" X:"+linear.getX()+" R:"+r);
        return new PointF(rateX,r);

    }


    @OnClick(R.id.cancel_faceTrack)
    public void cancelFaceTrack(){
        faceTrackLeft.setVisibility(View.GONE);
        faceTrackRight.setVisibility(View.GONE);
        mainLeftView.setVisibility(View.VISIBLE);
        mainRightView.setVisibility(View.VISIBLE);
        startFaceTrackBtn.setEnabled(true);
    }
    @OnClick(R.id.start_faceTrack)
    public void startFaceTrack(){
        startFaceTrackBtn.setEnabled(false);
    }
    @OnClick(R.id.face_track_start)
    public void gotoFaceTrackMode(){
        faceTrackLeft.setVisibility(View.VISIBLE);
        faceTrackRight.setVisibility(View.VISIBLE);
        mainLeftView.setVisibility(View.GONE);
        mainRightView.setVisibility(View.GONE);
    }
    @OnClick(R.id.apply_changes)
    public void applyChanges(){
        editleft.setVisibility(View.GONE);
        editRight.setVisibility(View.GONE);
    }

    @OnClick(R.id.cancel)
    public void cancelRecord(){
        recMode.setVisibility(View.GONE);
        recVideo.setVisibility(View.VISIBLE);
        stopRecVideo.setVisibility(View.GONE);
        mainLeftView.setVisibility(View.VISIBLE);
        mainRightView.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.goto_record_video)
    public void gotoRecordingMode(){
        recMode.setVisibility(View.VISIBLE);
        mainLeftView.setVisibility(View.GONE);
        mainRightView.setVisibility(View.GONE);
    }
    @OnClick(R.id.mute_btn)
    public void mute(){
        muteBtn.setVisibility(View.GONE);
        unMuteBtn.setVisibility(View.VISIBLE);
    }
    @OnClick(R.id.un_mute_btn)
    public void unMute(){
        muteBtn.setVisibility(View.VISIBLE);
        unMuteBtn.setVisibility(View.GONE);
    }
    @OnClick(R.id.record_video)
    public void startRecording(){
        recVideo.setVisibility(View.GONE);
        stopRecVideo.setVisibility(View.VISIBLE);
    }
    @OnClick(R.id.stop_record_video)
    public void setStopRecVideo(){
        recVideo.setVisibility(View.VISIBLE);
        stopRecVideo.setVisibility(View.GONE);
    }

    @OnClick(R.id.next_frame)
    public void showNextFrame(){

    }

    @OnClick(R.id.prev_frame)
    public void showPrevFrame(){

    }

    @OnClick(R.id.delete_now_and_after)
    public void deleteNowAndAfter(){

    }

    @OnClick(R.id.delete_now_and_before)
    public void deleteNowAndBefore(){

    }

    @OnClick(R.id.delete)
    public void deleteMask(){
        motionView.deletedSelectedEntity();
    }

    @OnClick(R.id.record_video)
    public void startRecordingVideo(){
        PointF point=getSeekBarPos(mHolderTopView,linear,mTimeLineContainer);
        leftPos=point.x;
    }

    @OnClick(R.id.stop_record_video)
    public void stopRecordingVideo(){
        PointF point=getSeekBarPos(mHolderTopView,linear,mTimeLineContainer);
        createBar(leftPos,point.x,point.y);
    }

    private void createBar(float left,float right,float top){
        //bars.add(new Bar(getApplicationContext(),leftPos,right));
        //Bitmap result = Bitmap.createBitmap((int)right-(int)left, 3, Bitmap.Config.ARGB_4444);
        //RectF rectF=new RectF();
        //Canvas canvas=new Canvas(result);
        Bar bar=new Bar(MainActivity.this,leftPos,right,top);
        //bar.draw(canvas);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int)right-(int)left,3);
        params.leftMargin =(int)left;
        params.topMargin = (int)top;
        //bar.setLayoutParams(params);
        //bar.setLayoutParams(new LinearLayout.LayoutParams((int)right-(int)left,5));
        scrollView.addView(bar);
        scrollView.postInvalidate();
        //setContentView(bar);
    }

}