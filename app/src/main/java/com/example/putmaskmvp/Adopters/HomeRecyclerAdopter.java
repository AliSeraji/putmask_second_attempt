package com.example.putmaskmvp.Adopters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.RequestManager;
import com.example.putmaskmvp.R;
import com.example.putmaskmvp.Utils.MediaObject;

import java.util.ArrayList;

public class HomeRecyclerAdopter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<MediaObject> mediaObjects;
    private RequestManager requestManager;
    private ArrayList<String> data;

    private Context mContext;
    public HomeRecyclerAdopter(ArrayList<MediaObject> mediaObjects,Context context,RequestManager requestManager){
        this.mediaObjects=mediaObjects;
        this.requestManager=requestManager;
        this.mContext=context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_post,viewGroup,false);

        return new HomeVideoPlayerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder homeHolder, int i) {

        ((HomeVideoPlayerViewHolder)homeHolder).onBind(mediaObjects.get(i),requestManager,this.mContext);
        ((HomeVideoPlayerViewHolder)homeHolder).setOnClickListeners();

    }

    @Override
    public int getItemCount() {
        return mediaObjects.size();
    }
/*
    public class HomeHolder extends RecyclerView.ViewHolder{
        private ImageView comment;
        private CircleImage circleImage;
        private SparkButton likeBtn;
        private TextView likedNum;
        public HomeHolder(@NonNull View itemView) {
            super(itemView);
            circleImage=itemView.findViewById(R.id.user_pic);
            comment=itemView.findViewById(R.id.add_comment);
            likeBtn=itemView.findViewById(R.id.likeBtn);
            likedNum=itemView.findViewById(R.id.like_num);
        }
    }*/
}
