package com.example.putmaskmvp.Adopters;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.RequestManager;
import com.example.putmaskmvp.CommentActivity;
import com.example.putmaskmvp.R;
import com.example.putmaskmvp.UseYourTemplate;
import com.example.putmaskmvp.Utils.MediaObject;
import com.example.putmaskmvp.ViewOthersProfile;
import com.jackandphantom.circularimageview.CircleImage;
import com.tuyenmonkey.mkloader.MKLoader;
import com.varunest.sparkbutton.SparkButton;

public class HomeVideoPlayerViewHolder extends RecyclerView.ViewHolder {
    private Context mContext;
    FrameLayout media_container;
    TextView title;
    ImageView thumbnail, volumeControl;
    MKLoader progressBar;
    View parent;
    RequestManager requestManager;
    private ImageView comment;
    private CircleImage circleImage;
    private SparkButton likeBtn;
    private TextView likedNum;
    private Button useYourTemplate;


    public HomeVideoPlayerViewHolder(@NonNull View itemView) {
        super(itemView);
        parent = itemView;
        media_container = itemView.findViewById(R.id.post_video_holder);
        thumbnail = itemView.findViewById(R.id.thumbnail);
        title = itemView.findViewById(R.id.title);
        progressBar = itemView.findViewById(R.id.video_loader_bar);
        volumeControl = itemView.findViewById(R.id.volume_control);
        circleImage=itemView.findViewById(R.id.user_pic);
        comment=itemView.findViewById(R.id.add_comment);
        likeBtn=itemView.findViewById(R.id.likeBtn);
        likedNum=itemView.findViewById(R.id.like_num);
        useYourTemplate=itemView.findViewById(R.id.use_template);
    }


    public void onBind(MediaObject mediaObject, RequestManager requestManager,Context context) {
        this.requestManager = requestManager;
        this.mContext=context;
        parent.setTag(this);
        //title.setText(mediaObject.getTitle());
        this.requestManager.load(mediaObject.getThumbnail()).into(thumbnail);

    }

    public void setOnClickListeners(){
        circleImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext, ViewOthersProfile.class);

                mContext.startActivity(intent);
            }
        });

        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext, CommentActivity.class);
                mContext.startActivity(intent);
            }
        });

        likeBtn.setOnClickListener(new View.OnClickListener() {
            Boolean liked=false;
            int num;
            String str;
            @Override
            public void onClick(View v) {
                if(!liked ){
                    likeBtn.playAnimation();
                    likeBtn.setChecked(true);
                    num=Integer.parseInt(likedNum.getText().toString());
                    num++;
                    str=new Integer(num).toString();
                    likedNum.setText(str);
                    liked=true;
                    //Log.i("tag","post num "+homeHolder.getAdapterPosition()+" is liked");
                }
                else if(liked){
                    likeBtn.setChecked(false);
                    num=Integer.parseInt(likedNum.getText().toString());
                    num--;
                    str=new Integer(num).toString();
                    likedNum.setText(str);
                    liked=false;
                    //Log.i("tag1","post num "+homeHolder.getAdapterPosition()+" is disliked");
                }
            }
        });

        useYourTemplate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext, UseYourTemplate.class);
                mContext.startActivity(intent);
            }
        });

    }
}