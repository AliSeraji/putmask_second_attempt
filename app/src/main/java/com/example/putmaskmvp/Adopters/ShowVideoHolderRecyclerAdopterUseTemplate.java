package com.example.putmaskmvp.Adopters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.putmaskmvp.R;

import java.util.ArrayList;

public class ShowVideoHolderRecyclerAdopterUseTemplate extends RecyclerView.Adapter<ShowVideoHolderRecyclerAdopterUseTemplate.VideoRecyclerHolderUseTemplate> {

    private ArrayList<ModelVideo> allVideo;
    private Context mContext;
    private OnVideoPickerClickListener onVideoPickerClickListener;

   public ShowVideoHolderRecyclerAdopterUseTemplate(Context context,ArrayList<ModelVideo> allVideo,OnVideoPickerClickListener onVideoPickerClickListener){
        this.mContext=context;
        this.allVideo=allVideo;
        this.onVideoPickerClickListener=onVideoPickerClickListener;
   }


    @NonNull
    @Override
    public VideoRecyclerHolderUseTemplate onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

       View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_pick_use_template,parent,false);
       return new VideoRecyclerHolderUseTemplate(view,onVideoPickerClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoRecyclerHolderUseTemplate holder, int position) {

        Glide.with(mContext).load(allVideo.get(position).getStr_thumb()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }
            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).apply(new RequestOptions().override(110,110))
                .skipMemoryCache(false).centerCrop().into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return allVideo.size();
    }

    public class VideoRecyclerHolderUseTemplate extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ImageView imageView;
        OnVideoPickerClickListener onVideoPickerClickListener;
        public VideoRecyclerHolderUseTemplate(@NonNull View itemView,OnVideoPickerClickListener onVideoPickerClickListener) {
            super(itemView);
            imageView=itemView.findViewById(R.id.image_view_pick_use_template);
            this.onVideoPickerClickListener=onVideoPickerClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onVideoPickerClickListener.onVideoClick(getAdapterPosition());
        }
    }

    public interface OnVideoPickerClickListener{
       void onVideoClick(int pos) ;
    }
}
