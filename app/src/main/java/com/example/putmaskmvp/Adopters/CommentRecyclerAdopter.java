package com.example.putmaskmvp.Adopters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.putmaskmvp.R;

public class CommentRecyclerAdopter extends RecyclerView.Adapter<CommentRecyclerAdopter.CommentHolder> {

    private int num=0;
    public CommentRecyclerAdopter(int num){
        this.num=num;

    }

    @NonNull
    @Override
    public CommentHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_comments,viewGroup,false);
        return new CommentHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentHolder commentHolder, int i) {

        String t="This is a comment made by this user on this post";
        String name="UserName"+i;
        commentHolder.username.setText(name);
        commentHolder.comment.setText(t);
    }

    @Override
    public int getItemCount() {
        return num;
    }

    public class CommentHolder extends RecyclerView.ViewHolder {

        public TextView username,comment;

        public CommentHolder(@NonNull View itemView) {
            super(itemView);
            comment=itemView.findViewById(R.id.user_comment);
            username=itemView.findViewById(R.id.user_name_commenter);
        }
    }
}
