package com.example.putmaskmvp.Adopters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.putmaskmvp.R;

import java.util.ArrayList;


public class ShowStickerHolderRecyclerAdopterUseTemplate extends RecyclerView.Adapter<ShowStickerHolderRecyclerAdopterUseTemplate.StickerKeeper> {

    private ArrayList<Integer> stickers;
    private OnStickerPickerClickListener onStickerPickerClickListener;
    private Context mContext;

    public ShowStickerHolderRecyclerAdopterUseTemplate(OnStickerPickerClickListener onStickerPickerClickListener, Context context,ArrayList<Integer> stickers){
        this.onStickerPickerClickListener=onStickerPickerClickListener;
        this.mContext=context;
        this.stickers=stickers;
    }

    @NonNull
    @Override
    public StickerKeeper onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_pick_sticker_use_template,parent,false);
        return new StickerKeeper(view,onStickerPickerClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull StickerKeeper holder, int position) {
        holder.imageView.setImageDrawable(ContextCompat.getDrawable(mContext,stickers.get(position)));
    }

    @Override
    public int getItemCount() {
        return stickers.size();
    }

    public class StickerKeeper extends RecyclerView.ViewHolder implements View.OnClickListener{
        OnStickerPickerClickListener onStickerPickerClickListener;
        public ImageView imageView;
        public StickerKeeper(@NonNull View itemView,OnStickerPickerClickListener onStickerPickerClickListener) {
            super(itemView);
            this.onStickerPickerClickListener=onStickerPickerClickListener;
            imageView=itemView.findViewById(R.id.image_view_pick_sticker_use_template);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onStickerPickerClickListener.onStickerClick(getAdapterPosition());
        }
    }
    public interface OnStickerPickerClickListener{
        void onStickerClick(int pos);
    }
}
