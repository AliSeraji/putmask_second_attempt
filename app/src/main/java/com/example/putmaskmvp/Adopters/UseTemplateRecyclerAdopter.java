package com.example.putmaskmvp.Adopters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.putmaskmvp.R;
import com.example.putmaskmvp.widget.entity.MotionEntity;

import java.util.List;

public class UseTemplateRecyclerAdopter extends RecyclerView.Adapter<UseTemplateRecyclerAdopter.StickerHolder> {


    private List<MotionEntity> motionEntities;
    private OnStickerHolderItemListener onStickerHolderItemListener;

    public UseTemplateRecyclerAdopter(OnStickerHolderItemListener onStickerHolderItemListener,List<MotionEntity> motionEntities){
        this.onStickerHolderItemListener=onStickerHolderItemListener;
        this.motionEntities=motionEntities;
    }

    @NonNull
    @Override
    public StickerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_use_template,parent,false);

        return new StickerHolder(view,onStickerHolderItemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull StickerHolder holder, int position) {
            holder.imageView.setImageBitmap(motionEntities.get(position).getEntityBitmap());

    }


    @Override
    public int getItemCount() {
        return motionEntities.size();

    }

    public class StickerHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ImageView imageView;
        OnStickerHolderItemListener onStickerHolderItemListener;

        public StickerHolder(@NonNull View itemView,OnStickerHolderItemListener onStickerHolderItemListener) {
            super(itemView);
            this.onStickerHolderItemListener=onStickerHolderItemListener;
            imageView=itemView.findViewById(R.id.stickerHolder);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onStickerHolderItemListener.OnItemClick(getAdapterPosition());
        }
    }

    public interface OnStickerHolderItemListener{
        void OnItemClick(int pos);
    }
}
