package com.example.putmaskmvp.Adopters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.example.putmaskmvp.Fragments.HomeFragment;
import com.example.putmaskmvp.Fragments.NotificationFragment;
import com.example.putmaskmvp.Fragments.ProfileFragment;
import com.example.putmaskmvp.Fragments.SearchFragment;
import com.example.putmaskmvp.Fragments.ShareFragment;
import com.example.putmaskmvp.SocialActivity;

public class PagerAdopter extends FragmentPagerAdapter {

    private static int PAGES=5;
    SparseArray<Fragment> registeredFragments = new SparseArray<>();

    private SocialActivity socialActivity;

    public PagerAdopter(FragmentManager fm, SocialActivity socialActivity) {
        super(fm);
        this.socialActivity=socialActivity;

    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return HomeFragment.newInstance("param1","param2");
            case 1:
                return SearchFragment.newInstance("param1","param2");
            case 2:
                return ShareFragment.newInstance("param1","param2");
            case 3:
                return NotificationFragment.newInstance("param1","param2");
            case 4:
                return ProfileFragment.newInstance("param1","param2");
            default:
                return HomeFragment.newInstance("param1","param2");

        }
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

    @Override
    public long getItemId(int pos){
        return  System.currentTimeMillis();

    }


    @Override
    public int getCount() {
        return PAGES;
    }
}

