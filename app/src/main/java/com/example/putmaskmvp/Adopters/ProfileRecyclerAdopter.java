package com.example.putmaskmvp.Adopters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.putmaskmvp.R;

public class ProfileRecyclerAdopter  extends RecyclerView.Adapter<ProfileRecyclerAdopter.ProfileRecyclerHolder> {

    private Context mContext;
    private int numPosts=0;
    private OnProfileRecyclerItemListener onProfileRecyclerItemListener;
    public ProfileRecyclerAdopter(Context context, int numPosts,OnProfileRecyclerItemListener onProfileRecyclerItemListener){
        this.mContext=context;
        this.numPosts=numPosts;
        this.onProfileRecyclerItemListener=onProfileRecyclerItemListener;

    }
    @NonNull
    @Override
    public ProfileRecyclerAdopter.ProfileRecyclerHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_profile_posts,viewGroup,false);
        return new ProfileRecyclerHolder(view,onProfileRecyclerItemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileRecyclerAdopter.ProfileRecyclerHolder profileHolder, int position) {

    }
    @Override
    public int getItemCount() {
        return numPosts;
    }

    public class ProfileRecyclerHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        OnProfileRecyclerItemListener onProfileRecyclerItemListener;

        public ProfileRecyclerHolder(@NonNull View itemView,OnProfileRecyclerItemListener onProfileRecyclerItemListener) {
            super(itemView);
            this.onProfileRecyclerItemListener=onProfileRecyclerItemListener;
            itemView.setOnClickListener(this);

        }
        @Override
        public void onClick(View v) {
            onProfileRecyclerItemListener.OnItemClick(getAdapterPosition());
        }
    }

    public interface OnProfileRecyclerItemListener{
        void OnItemClick(int position);
    }
}

