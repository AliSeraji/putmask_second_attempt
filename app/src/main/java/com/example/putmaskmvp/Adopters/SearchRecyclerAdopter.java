package com.example.putmaskmvp.Adopters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.putmaskmvp.R;

import java.util.ArrayList;

public class SearchRecyclerAdopter extends RecyclerView.Adapter<SearchRecyclerAdopter.SearchHolder> {

    private ArrayList<String> result;


    public SearchRecyclerAdopter(ArrayList<String> result){
        this.result=result;
    }

    @NonNull
    @Override
    public SearchHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_search,parent,false);
        return new SearchHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchHolder holder, int position) {
        holder.textView.setText(result.get(position));
    }

    @Override
    public int getItemCount() {
        return result.size();
    }

    public class SearchHolder extends RecyclerView.ViewHolder{
        TextView textView;
        public SearchHolder(View itemView) {
            super(itemView);
            textView=itemView.findViewById(R.id.user_name_searched_in_search);
        }
    }
}
