package com.example.putmaskmvp.Adopters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.putmaskmvp.Fragments.DraftFragment;
import com.example.putmaskmvp.Fragments.PostedFragment;

public class ProfilePagerAdopter extends FragmentPagerAdapter {
    private final int PAGES=2;

    public ProfilePagerAdopter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0:
                return PostedFragment.newInstance("param1","param2");
            case 1:
                return DraftFragment.newInstance("param1","param2");
            default:
                return null;
        }
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

    @Override
    public long getItemId(int pos){
        return  System.currentTimeMillis();

    }

    @Override
    public int getCount() {
        return PAGES;
    }
}