package com.example.putmaskmvp.Adopters;

import android.content.DialogInterface;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import com.example.putmaskmvp.R;
import com.example.putmaskmvp.SaveVideoActivity;

public class SpinnerOnItemSelectedListener implements OnItemSelectedListener {

    private SaveVideoActivity saveVideoActivity;//to get the activity
    private int previousItemId=-1;//to avoid dialogue show up in first time
    public SpinnerOnItemSelectedListener(SaveVideoActivity saveVideoActivity){
        this.saveVideoActivity=saveVideoActivity;
    }

    //to check the dropdown list selected item
    public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
        if(previousItemId==-1){
            previousItemId=0;
            return;
        }

        //alert user for custom privacy
        if(parent.getItemAtPosition(pos).toString().equals("Custom")){
            createAlertDialogue();
        }
        else {
            //do what ever you want with those two choices
            createSaveVideoDialogue();
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }

    //create alert dialogue
    private void createAlertDialogue(){
        AlertDialog.Builder builder = new AlertDialog.Builder(saveVideoActivity);

        TextView title = new TextView(saveVideoActivity);
        // Title Properties
        title.setText("Attention");
        title.setPadding(10, 10, 10, 30);   // Set Position
        title.setGravity(Gravity.CENTER);
        title.setTextColor(Color.BLACK);
        title.setTextSize(20);

        //set dialogue title
        builder.setCustomTitle(title);

        // Set Message
        TextView msg = new TextView(saveVideoActivity);

        // Message Properties
        msg.setTextSize(17);
        msg.setPadding(10,0,0,10);
        msg.setText("If you want to use custom privacy charges will be applied.\n Proceed?");
        msg.setGravity(Gravity.CENTER_HORIZONTAL);
        msg.setTextColor(Color.BLACK);
        builder.setView(msg);

        // Add the buttons
        builder.setPositiveButton(R.string.Ok, new DialogInterface.OnClickListener() {
            // User clicked OK button
            public void onClick(DialogInterface dialog, int id) {
                createSaveVideoDialogue();
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            // User cancelled the dialog
            public void onClick(DialogInterface dialog, int id) {
                return;
            }
        });

        // Create and show the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_PANEL);
        dialog.show();

        // Set Properties for OK Button
        final Button okBT = dialog.getButton(AlertDialog.BUTTON_NEUTRAL);
        LinearLayout.LayoutParams neutralBtnLP = (LinearLayout.LayoutParams) okBT.getLayoutParams();
        neutralBtnLP.gravity = Gravity.FILL_HORIZONTAL;
        okBT.setPadding(50, 10, 10, 10);   // Set Position
        okBT.setTextColor(Color.BLUE);
        okBT.setLayoutParams(neutralBtnLP);

        //Set Properties for cancel Button
        final Button cancelBT = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        LinearLayout.LayoutParams negBtnLP = (LinearLayout.LayoutParams) okBT.getLayoutParams();
        negBtnLP.gravity = Gravity.FILL_HORIZONTAL;
        cancelBT.setTextColor(Color.RED);
        cancelBT.setLayoutParams(negBtnLP);

    }

    // create a dialogue while
    public void createSaveVideoDialogue(){

        AlertDialog.Builder builder = new AlertDialog.Builder(saveVideoActivity);

        // Get the layout inflater
        LayoutInflater inflater = saveVideoActivity.getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        //do what ever you want in dialogue layout
        //put,find anything in dialog_build_video
        builder.setView(inflater.inflate(R.layout.dialog_build_video, null))
                // Add action buttons
                .setPositiveButton(R.string.build_video, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                      //user accepts building video
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //user cancels building video
                    }
                });
        //Set Properties for cancel Button
        builder.show();
    }

}