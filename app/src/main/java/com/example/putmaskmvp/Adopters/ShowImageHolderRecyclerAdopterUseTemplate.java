package com.example.putmaskmvp.Adopters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.putmaskmvp.R;

import java.util.ArrayList;

public class ShowImageHolderRecyclerAdopterUseTemplate extends RecyclerView.Adapter<ShowImageHolderRecyclerAdopterUseTemplate.ImageHolderUseTemplate> {

    private ArrayList<String> imagePaths;
    private OnImagePickerClickListener onImagePickerClickListener;
    private Context mContext;
    public ShowImageHolderRecyclerAdopterUseTemplate(OnImagePickerClickListener onImagePickerClickListener,ArrayList<String> imagePaths,Context context){
        this.onImagePickerClickListener=onImagePickerClickListener;
        this.imagePaths=imagePaths;
        this.mContext=context;
    }

    @NonNull
    @Override
    public ImageHolderUseTemplate onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_pick_image_use_template,parent,false);
        return new ImageHolderUseTemplate(view,onImagePickerClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageHolderUseTemplate holder, int position) {

        Glide.with(mContext).load(imagePaths.get(position)).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }
            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).apply(new RequestOptions().override(100,100))
                .skipMemoryCache(false).centerCrop().into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return imagePaths.size();
    }

    public class ImageHolderUseTemplate extends RecyclerView.ViewHolder implements View.OnClickListener{

        OnImagePickerClickListener onImagePickerClickListener;
        public ImageView imageView;
        public ImageHolderUseTemplate(@NonNull View itemView,OnImagePickerClickListener onImagePickerClickListener) {
            super(itemView);
            imageView=itemView.findViewById(R.id.image_view_pick_image_use_template);
            this.onImagePickerClickListener=onImagePickerClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onImagePickerClickListener.onImageClick(getAdapterPosition());
        }
    }
    public interface OnImagePickerClickListener{
        void onImageClick(int pos);
    }
}
