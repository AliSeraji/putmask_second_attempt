package com.example.putmaskmvp.Adopters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.putmaskmvp.R;

public class ViewProfileRecyclerAdopter extends RecyclerView.Adapter<ViewProfileRecyclerAdopter.ViewProfileHolder> {

    private int numOfItems=0;

    public ViewProfileRecyclerAdopter(int numOfItems){
        this.numOfItems=numOfItems;
    }
    @NonNull
    @Override
    public ViewProfileHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_profile_posts,viewGroup,false);
        return new ViewProfileHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewProfileHolder viewProfileHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return numOfItems;
    }

    public class ViewProfileHolder extends RecyclerView.ViewHolder {


        public ViewProfileHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
