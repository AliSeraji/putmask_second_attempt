package com.example.putmaskmvp.Adopters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.putmaskmvp.R;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

public class ShareRecyclerAdopter extends RecyclerView.Adapter<ShareRecyclerAdopter.ShareHolder>  {
    private String path=null;
    private ArrayList<ModelVideo> allVideo;
    private Context mContext;
    private SimpleExoPlayer player;
    private ImageView pausePlay;
    public ShareRecyclerAdopter(Context context, ArrayList<ModelVideo> allVideo, SimpleExoPlayer player,ImageView pausePlay){
        this.allVideo=allVideo;
        this.mContext=context;
        this.player=player;
        this.pausePlay=pausePlay;
    }

    @NonNull
    @Override
    public ShareHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_share,viewGroup,false);
        return new ShareHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ShareHolder viewHolder, final int position) {

        Glide.with(mContext).load(allVideo.get(position).getStr_thumb()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).apply(new RequestOptions().override(110,110))
                .skipMemoryCache(false).centerCrop().into(viewHolder.imageView);

        viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pausePlay.setVisibility(View.GONE);
                setUpPlayer(allVideo.get(viewHolder.getAdapterPosition()).getStr_path());
                path=allVideo.get(viewHolder.getAdapterPosition()).getStr_path();
            }
        });
    }

    @Override
    public int getItemCount() {
        return allVideo.size();
    }

    public class ShareHolder extends RecyclerView.ViewHolder{
        public ImageView imageView;
        public AVLoadingIndicatorView avLoadingIndicatorView;
        public ShareHolder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.share_video);
            avLoadingIndicatorView=itemView.findViewById(R.id.avi);
        }
    }
    public String getPath(){
        return path;
    }


    private void setUpPlayer(String videoUrl) {
        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(
                mContext,videoUrl);
        MediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .setExtractorsFactory(new DefaultExtractorsFactory())
                .createMediaSource(Uri.parse(videoUrl));
        player.prepare(mediaSource);
        player.setPlayWhenReady(true);
        player.seekTo(0);
    }
}
