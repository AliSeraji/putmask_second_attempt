package com.example.putmaskmvp;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.example.putmaskmvp.Adopters.SpinnerOnItemSelectedListener;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SaveVideoActivity extends AppCompatActivity {

    private Spinner dropDownList;//dropdown menu
    private SpinnerOnItemSelectedListener spinnerOnItemSelectedListener;//spinner actions are defined in it

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_video_acivity);
        ButterKnife.bind(this);
        addListenerOnSpinnerItemSelection();
    }

    //create and ready drop down list
    public void addListenerOnSpinnerItemSelection() {
        dropDownList=  findViewById(R.id.spinner1);
        spinnerOnItemSelectedListener=new SpinnerOnItemSelectedListener(this);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.save_video_choices, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spiner_dropdown_layout);
        dropDownList.setAdapter(adapter);
        dropDownList.setOnItemSelectedListener(spinnerOnItemSelectedListener);

    }

    // get the selected dropdown list value
    @OnClick(R.id.build_video)
    public void buildVideo(){
        //if not custom privacy
        if(dropDownList.getSelectedItemPosition()!=2)
            spinnerOnItemSelectedListener.createSaveVideoDialogue();
    }


    //go back to UseTemplate Page
    @OnClick(R.id.back_to_use_your_template)
    public void backToUseTemplateActivity(){
        //Intent intent=new Intent(this,UseYourTemplate.class);
        //startActivity(intent);
        finish();
    }


}
