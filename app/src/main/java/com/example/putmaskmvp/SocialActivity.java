package com.example.putmaskmvp;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import com.androidnetworking.AndroidNetworking;
import com.example.putmaskmvp.Adopters.PagerAdopter;
import com.example.putmaskmvp.Adopters.ZoomOutPageTransformer;
import com.gauravk.bubblenavigation.BubbleNavigationConstraintView;
import com.gauravk.bubblenavigation.listener.BubbleNavigationChangeListener;
import java.util.List;

public class SocialActivity extends AppCompatActivity {

    private boolean doubleBackToExitPressedOnce = false;
    private BubbleNavigationConstraintView bubbleNavigationConstraintView;
    private ViewPager viewPager;
    private PagerAdopter pagerAdapter;
    static final String EXTRA_VIDEO_PATH = "EXTRA_VIDEO_PATH";
    static final String VIDEO_TOTAL_DURATION = "VIDEO_TOTAL_DURATION";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);
        bubbleNavigationConstraintView=findViewById(R.id.bottom_navigation_constraint);
        viewPager=findViewById(R.id.view_pager);
        pagerAdapter=new PagerAdopter(getSupportFragmentManager(),SocialActivity.this);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setPageTransformer(true,new ZoomOutPageTransformer());


        setUpListeners();
        AndroidNetworking.initialize(getApplicationContext());
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please press BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    private void setUpListeners(){
        bubbleNavigationConstraintView.setNavigationChangeListener(new BubbleNavigationChangeListener() {
            @Override
            public void onNavigationChanged(View view, int position) {
                viewPager.setCurrentItem(position,true);
            }
        });


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            int currentPos=0;

            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                final Fragment pausedFragment = pagerAdapter.getRegisteredFragment(currentPos);
                pausedFragment.onPause();
                if(currentPos==4)
                    pausedFragment.onStop();
                currentPos=position;
                final Fragment resumedFragment = pagerAdapter.getRegisteredFragment(currentPos);
                resumedFragment.onResume();
                if(currentPos==4)
                    resumedFragment.onStart();
                bubbleNavigationConstraintView.setCurrentActiveItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        final List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }





}
