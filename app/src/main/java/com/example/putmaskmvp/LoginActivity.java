package com.example.putmaskmvp;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.google.android.material.textfield.TextInputEditText;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private Guideline rightGuidLine,leftGuidLine;
    private boolean isSigninScreen = true;
    private TextView tvSignupInvoker;
    private LinearLayout llSignup;
    private TextView tvSigninInvoker,faceBooktxt,instatxt,googletxt;
    private LinearLayout llSignin;
    private Button btnSignup;
    private Button btnSignin;
    private Button guest;
    private RelativeLayout google,insta,faceBook;
    private LinearLayout llsignup;
    private ImageView googleIcon,faceBookkIcon,instaIcon;
    private TextInputEditText emailInput,passwordInput;
    private MKLoader loginLoader,guestLoader,googleLoader,faceBookLoader,instaLoader,signUpLoader;
    private boolean clickable=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        AndroidNetworking.initialize(getApplicationContext());
        rightGuidLine=findViewById(R.id.right_guideline);
        leftGuidLine=findViewById(R.id.left_guideline);
        llSignin =  findViewById(R.id.llSignin);
        llSignin.setOnClickListener(this);
        /////
        instaLoader=findViewById(R.id.insta_loading);
        faceBookLoader=findViewById(R.id.face_book_loading);
        googleLoader=findViewById(R.id.google_loading);
        guestLoader=findViewById(R.id.guest_loading);
        loginLoader=findViewById(R.id.loading_loading);
        signUpLoader=findViewById(R.id.signup_loading);

        emailInput=findViewById(R.id.email_input);
        passwordInput=findViewById(R.id.password_input);

        instaIcon=findViewById(R.id.instagram_logo_container);
        faceBookkIcon=findViewById(R.id.face_book_logo_container);
        googleIcon=findViewById(R.id.google_logo_container);

        instatxt=findViewById(R.id.insta_txt);
        faceBooktxt=findViewById(R.id.face_book_txt);
        googletxt=findViewById(R.id.google_txt);

        insta=findViewById(R.id.login_with_instagram);
        google=findViewById(R.id.login_with_google);
        faceBook=findViewById(R.id.login_with_face_book);
        guest=findViewById(R.id.btnGuest);


        ///////
        llsignup =findViewById(R.id.llSignup);
        llsignup.setOnClickListener(this);
        tvSignupInvoker = findViewById(R.id.tvSignupInvoker);
        tvSigninInvoker = findViewById(R.id.tvSigninInvoker);

        btnSignup=  findViewById(R.id.btnSignup);
        btnSignin= findViewById(R.id.btnSignin);

        llSignup =  findViewById(R.id.llSignup);
        llSignin =  findViewById(R.id.llSignin);

        initSpinners();
        showSigninForm();
        setClickListeners();

    }
    private void showSignupForm() {
        ConstraintLayout.LayoutParams paramsLogin = (ConstraintLayout.LayoutParams) llSignin.getLayoutParams();
        paramsLogin.matchConstraintPercentWidth=0.15F;

        llSignin.requestLayout();


        ConstraintLayout.LayoutParams paramsSignup = (ConstraintLayout.LayoutParams) llSignup.getLayoutParams();

        paramsSignup.matchConstraintPercentWidth=0.85F;

        llSignup.requestLayout();

        tvSignupInvoker.setVisibility(View.GONE);
        tvSigninInvoker.setVisibility(View.VISIBLE);
        Animation translate= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.translate_right_to_left);
        llSignup.startAnimation(translate);


        Animation clockwise= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_right_to_left);
        btnSignup.startAnimation(clockwise);

    }

    private void showSigninForm() {
        ConstraintLayout.LayoutParams paramsLogin = (ConstraintLayout.LayoutParams) llSignin.getLayoutParams();
        paramsLogin.matchConstraintPercentWidth=0.85F;

        llSignin.requestLayout();


        ConstraintLayout.LayoutParams paramsSignup = (ConstraintLayout.LayoutParams) llSignup.getLayoutParams();
        paramsSignup.matchConstraintPercentWidth=0.15F;

        //int x=leftGuidLine.getRight();
        llSignup.requestLayout();

        Animation translate= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.translate_left_to_right);
        llSignin.startAnimation(translate);

        tvSignupInvoker.setVisibility(View.VISIBLE);
        tvSigninInvoker.setVisibility(View.GONE);
        Animation clockwise= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_left_to_right);
        startAnimations(clockwise);
        startAnimations(clockwise);

    }

    private void startAnimations(Animation anim){
        guest.startAnimation(anim);
        faceBook.startAnimation(anim);
        google.setAnimation(anim);
        insta.startAnimation(anim);
        btnSignin.startAnimation(anim);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.llSignin || v.getId() ==R.id.llSignup){
            InputMethodManager methodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        }
    }

    public void initSpinners(){
        loginLoader.setVisibility(View.GONE);
        guestLoader.setVisibility(View.GONE);
        faceBookLoader.setVisibility(View.GONE);
        googleLoader.setVisibility(View.GONE);
        instaLoader.setVisibility(View.GONE);
        signUpLoader.setVisibility(View.GONE);
        clickable=true;
    }

    public void setClickListeners(){
        tvSignupInvoker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(clickable){
                    isSigninScreen = false;
                    showSignupForm();
                }
            }
        });

        tvSigninInvoker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(clickable) {
                    isSigninScreen = true;
                    showSigninForm();
                }
            }
        });


        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(clickable) {
                    clickable=false;
                    Animation clockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_right_to_left);
                    if (isSigninScreen)
                        btnSignup.startAnimation(clockwise);
                    signUpLoader.setVisibility(View.VISIBLE);
                    Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
                    btnSignup.startAnimation(anim);
                    btnSignup.setText(null);
                    signUpLoader.startAnimation(anim);
                    CountDownTimer cDown = new CountDownTimer(3000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            gotoSocialActivity();
                        }
                    }.start();
                }
            }
        });

        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clickable) {
                    clickable=false;
                    Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
                    google.startAnimation(anim);
                    googleLoader.setVisibility(View.VISIBLE);
                    googleLoader.startAnimation(anim);
                    googletxt.setText(null);
                    CountDownTimer cDown = new CountDownTimer(3000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            gotoSocialActivity();
                        }
                    }.start();
                }
            }
        });

        insta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clickable) {
                    clickable=false;
                    instaLoader.setVisibility(View.VISIBLE);
                    Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
                    insta.startAnimation(anim);
                    instaLoader.startAnimation(anim);
                    instatxt.setText(null);
                    CountDownTimer cDown = new CountDownTimer(3000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            gotoSocialActivity();
                        }
                    }.start();

                }
            }
        });

        faceBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clickable) {
                    clickable=false;
                    faceBookLoader.setVisibility(View.VISIBLE);
                    Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
                    faceBook.startAnimation(anim);
                    faceBookLoader.startAnimation(anim);
                    faceBooktxt.setText(null);
                    CountDownTimer cDown = new CountDownTimer(3000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            gotoSocialActivity();
                        }
                    }.start();
                }
            }
        });

        guest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clickable) {
                    clickable=false;
                    guestLoader.setVisibility(View.VISIBLE);
                    Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
                    guest.startAnimation(anim);
                    guestLoader.startAnimation(anim);
                    guest.setText(null);
                    CountDownTimer cDown = new CountDownTimer(3000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            gotoSocialActivity();
                        }
                    }.start();
                }
            }
        });

        btnSignin.setOnClickListener(new View.OnClickListener() {
            User user=new User();
            @Override
            public void onClick(View v) {
                if(clickable) {
                    clickable=false;
                    loginLoader.setVisibility(View.VISIBLE);
                    Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
                    btnSignin.startAnimation(anim);
                    loginLoader.startAnimation(anim);
                    btnSignin.setText(null);
                    //user.setEmail(emailInput.getText().toString());
                    //user.setPassWord(passwordInput.getText().toString());
                    tryLogin();
                    /*CountDownTimer cDown = new CountDownTimer(3000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }
                        @Override
                        public void onFinish() {




                            gotoSocialActivity();
                        }
                    }.start();*/
                }
            }
        });
    }

    private void gotoSocialActivity(){
        Intent intent=new Intent(this,SocialActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void finish(){
        super.finish();
        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
    }

    private class User{
        private String passWord;
        private String email;
        public void setPassWord(String str){passWord=str;}
        public void setEmail(String str){email=str;}
        public String getPassWord(){return passWord;}
        public String getEmail(){return email;}
    }

    private void tryLogin(){
       /* AndroidNetworking.post("https://api.github.com/users")
                .addBodyParameter(null) // posting java object
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // do anything with response
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });*/
        AndroidNetworking.get("https://api.github.com/users")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        int x=response.length();
                        Log.d("size",""+x);
                        for(int i=0;i<response.length();i++){
                            try {
                                JSONObject jb=response.getJSONObject(i);
                                Log.d("tag",jb.getString("login"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }



}



