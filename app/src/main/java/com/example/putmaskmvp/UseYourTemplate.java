package com.example.putmaskmvp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.putmaskmvp.Adopters.FontsAdapter;
import com.example.putmaskmvp.Adopters.ModelVideo;
import com.example.putmaskmvp.Adopters.ShowImageHolderRecyclerAdopterUseTemplate;
import com.example.putmaskmvp.Adopters.ShowStickerHolderRecyclerAdopterUseTemplate;
import com.example.putmaskmvp.Adopters.ShowVideoHolderRecyclerAdopterUseTemplate;
import com.example.putmaskmvp.Adopters.UseTemplateRecyclerAdopter;
import com.example.putmaskmvp.Utils.FontProvider;
import com.example.putmaskmvp.viewmodel.Font;
import com.example.putmaskmvp.viewmodel.Layer;
import com.example.putmaskmvp.viewmodel.TextLayer;
import com.example.putmaskmvp.widget.MotionView;
import com.example.putmaskmvp.widget.entity.ImageEntity;
import com.example.putmaskmvp.widget.entity.MotionEntity;
import com.example.putmaskmvp.widget.entity.TextEntity;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UseYourTemplate extends AppCompatActivity implements TextEditorDialogFragment.OnTextLayerCallback, SurfaceHolder.Callback, UseTemplateRecyclerAdopter.OnStickerHolderItemListener,
ShowVideoHolderRecyclerAdopterUseTemplate.OnVideoPickerClickListener,ShowImageHolderRecyclerAdopterUseTemplate.OnImagePickerClickListener, ShowStickerHolderRecyclerAdopterUseTemplate.OnStickerPickerClickListener {

    private int SELECT_VIDEO_REQUEST_CODE = 10;
    private int SELECT_IMAGE_REQUEST_CODE = 11;
    private static final int REQUEST_STORAGE_IMAGE_ACCESS_PERMISSION=109;
    private static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    public static final int SELECT_STICKER_REQUEST_CODE = 123;
    private ArrayList<ModelVideo> allVideo = new ArrayList<>();


    private FontProvider fontProvider;
    private Uri videoUri=null, imageUri;
    protected MotionView motionView;
    protected View textEntityEditPanel;
    private SurfaceView surfaceView;
    private MediaPlayer mediaPlayer;
    private SurfaceHolder surfaceHolder;
    private Thread thread;
    private Runnable runnable;
    private Handler mHandler = new Handler();
    private boolean isInterrupted;
    private boolean isClickable=true;
    private ArrayList<String> listOfAllImages = new ArrayList<String>();
    private long videoDuration = 0;
    private Button gotoProcessView;
    private Button seeTemplate;

    @BindView(R.id.seek_bar_use_template)
    SeekBar mHolderTopView;//seekbar itself

    @BindView(R.id.play_button_use_template)
    ImageButton playBtn;

    @BindView(R.id.pause_button_use_template)
    ImageButton pauseBtn;

    @BindView(R.id.show_video_holder_recycler_holder_use_template)
    RelativeLayout videoPickerHolder;//a relative layout which has a recyclerview to show device videos in it

    @BindView(R.id.show_sticker_holder_use_template)
    RelativeLayout stickerPickerHolder;//a relative layout which has a reyclerview to show stickers in it

    @BindView(R.id.show_image_holder_use_template)
    RelativeLayout imagePickerHolder;//a realtive layout to which has a recyclerview to show images in it

    @BindView(R.id.video_timer_use_template)
    TextView elapsedTime;//shows elapsed time if video

    @BindView(R.id.unmute_video)ImageButton unmuteBtn;
    @BindView(R.id.mute_video)ImageButton muteBtn;


    private UseTemplateRecyclerAdopter useTemplateRecyclerAdopter;
    private ShowVideoHolderRecyclerAdopterUseTemplate showVideoHolderRecyclerAdopterUseTemplate;//a recycler adopter to show videos
    private ShowImageHolderRecyclerAdopterUseTemplate showImageHolderRecyclerAdopterUseTemplate;//a recycler adopter to show images

    //@BindView(R.id.recycler_view_use_template)
    //RecyclerView recyclerView;
    private RecyclerView recyclerView,videoPicker,stickerPicker,imagePicker;


    private int[] stickerIds = {
            R.drawable.abra,
            R.drawable.bellsprout,
            R.drawable.bracelet,
            R.drawable.bullbasaur,
            R.drawable.camera,
            R.drawable.candy,
            R.drawable.caterpie,
            R.drawable.charmander,
            R.drawable.mankey,
            R.drawable.map,
            R.drawable.mega_ball,
            R.drawable.meowth,
            R.drawable.pawprints,
            R.drawable.pidgey,
            R.drawable.pikachu,
            R.drawable.pikachu_1,
            R.drawable.pikachu_2,
            R.drawable.player,
            R.drawable.pointer,
            R.drawable.pokebag,
            R.drawable.pokeball,
            R.drawable.pokeballs,
            R.drawable.pokecoin,
            R.drawable.pokedex,
            R.drawable.potion,
            R.drawable.psyduck,
            R.drawable.rattata,
            R.drawable.revive,
            R.drawable.squirtle,
            R.drawable.star,
            R.drawable.star_1,
            R.drawable.superball,
            R.drawable.tornado,
            R.drawable.venonat,
            R.drawable.weedle,
            R.drawable.zubat
    };



    private final MotionView.MotionViewCallback motionViewCallback = new MotionView.MotionViewCallback() {
        @Override
        public void onEntitySelected(@Nullable MotionEntity entity) {
            if (entity instanceof TextEntity) {
                //textEntityEditPanel.setVisibility(View.VISIBLE);
            } else {
                //textEntityEditPanel.setVisibility(View.GONE);
            }
        }

        @Override
        public void onEntityDoubleTap(@NonNull MotionEntity entity) {

            startTextEntityEditing();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_use_your_template);
        ButterKnife.bind(this);
        initializeSurfaceView();
        motionView = findViewById(R.id.motion_view_use_template);
        recyclerView=findViewById(R.id.recycler_view_use_template);
        videoPicker=findViewById(R.id.video_recycler_adopter_use_template);
        stickerPicker=findViewById(R.id.sticker_recycler_adopter_use_template);
        imagePicker=findViewById(R.id.image_recycler_adopter_use_template);
        gotoProcessView=findViewById(R.id.goto_process_view);
        seeTemplate=findViewById(R.id.show_template);
        textEntityEditPanel = findViewById(R.id.main_motion_text_entity_edit_panel);
        initTextEntitiesListeners();
        this.fontProvider = new FontProvider(getResources());
        motionView.setMotionViewCallback(motionViewCallback);
        setupRecyclerView();
        isInterrupted=false;

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mediaPlayer.release();
        isInterrupted=true;

    }

    @Override
    protected void onStop(){
        super.onStop();
        mediaPlayer.pause();
        isInterrupted=true;
    }

    @Override
    protected void onResume(){
        super.onResume();
        isInterrupted=false;


    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mediaPlayer.setDisplay(holder);
        if (videoUri != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.reset();
                try {
                    mediaPlayer.setDataSource(this.getApplicationContext(), videoUri);
                    mediaPlayer.prepare();
                    mediaPlayer.setDisplay(surfaceHolder);
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    videoDuration=mediaPlayer.getDuration();
                    setUpTimeLinearContainer();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    mediaPlayer.setDataSource(this.getApplicationContext(), videoUri);
                    mediaPlayer.prepare();
                    mediaPlayer.setDisplay(surfaceHolder);
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    videoDuration=mediaPlayer.getDuration();
                    setUpTimeLinearContainer();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }


    private void initializeSurfaceView() {
        surfaceView = findViewById(R.id.surface_view_use_template);
        mediaPlayer = new MediaPlayer();
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceView.setKeepScreenOn(true);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mediaPlayer.seekTo(0);
                        mHolderTopView.setProgress(0);
                        mediaPlayer.start();
                    }
                });
    }

    private void addSticker(final int stickerResId) {

        motionView.post(new Runnable() {
            @Override
            public void run() {
                Layer layer = new Layer();
                Bitmap pica = BitmapFactory.decodeResource(getResources(), stickerResId);
                ImageEntity entity = new ImageEntity(layer, pica, motionView.getWidth(), motionView.getHeight());
                motionView.addEntityAndPosition(entity);
                useTemplateRecyclerAdopter.notifyDataSetChanged();
            }
        });
    }

    private void addImage(final Bitmap pic) {
        motionView.post(new Runnable() {
            @Override
            public void run() {
                //BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri));
                Layer layer = new Layer();
                ImageEntity entity = new ImageEntity(layer, pic, motionView.getWidth(), motionView.getHeight());
                motionView.addEntityAndPosition(entity);
                useTemplateRecyclerAdopter.notifyDataSetChanged();
            }
        });
    }


    private void initTextEntitiesListeners() {
        findViewById(R.id.text_entity_font_size_increase).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                increaseTextEntitySize();
            }
        });
        findViewById(R.id.text_entity_font_size_decrease).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                decreaseTextEntitySize();
            }
        });
        findViewById(R.id.text_entity_color_change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeTextEntityColor();
            }
        });
        findViewById(R.id.text_entity_font_change).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeTextEntityFont();
            }
        });
        findViewById(R.id.text_entity_edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startTextEntityEditing();
            }
        });
    }

    private void increaseTextEntitySize() {
        TextEntity textEntity = currentTextEntity();
        if (textEntity != null) {
            textEntity.getLayer().getFont().increaseSize(TextLayer.Limits.FONT_SIZE_STEP);
            textEntity.updateEntity();
            motionView.invalidate();
            useTemplateRecyclerAdopter.notifyDataSetChanged();
        }
    }

    private void decreaseTextEntitySize() {
        TextEntity textEntity = currentTextEntity();
        if (textEntity != null) {
            textEntity.getLayer().getFont().decreaseSize(TextLayer.Limits.FONT_SIZE_STEP);
            textEntity.updateEntity();
            motionView.invalidate();
            useTemplateRecyclerAdopter.notifyDataSetChanged();
        }
    }

    private void changeTextEntityColor() {
        TextEntity textEntity = currentTextEntity();
        if (textEntity == null) {
            return;
        }

        int initialColor = textEntity.getLayer().getFont().getColor();

        ColorPickerDialogBuilder
                .with(UseYourTemplate.this)
                .setTitle(R.string.select_color)
                .initialColor(initialColor)
                .wheelType(ColorPickerView.WHEEL_TYPE.CIRCLE)
                .density(8) // magic number
                .setPositiveButton(R.string.Ok, new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        TextEntity textEntity = currentTextEntity();
                        if (textEntity != null) {
                            textEntity.getLayer().getFont().setColor(selectedColor);
                            textEntity.updateEntity();
                            motionView.invalidate();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .build()
                .show();
        useTemplateRecyclerAdopter.notifyDataSetChanged();
    }

    private void changeTextEntityFont() {
        final List<String> fonts = fontProvider.getFontNames();
        FontsAdapter fontsAdapter = new FontsAdapter(this, fonts, fontProvider);
        new AlertDialog.Builder(this)
                .setTitle(R.string.select_font)
                .setAdapter(fontsAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        TextEntity textEntity = currentTextEntity();
                        if (textEntity != null) {
                            textEntity.getLayer().getFont().setTypeface(fonts.get(which));
                            textEntity.updateEntity();
                            motionView.invalidate();
                        }
                    }
                })
                .show();
        useTemplateRecyclerAdopter.notifyDataSetChanged();
    }

    private void startTextEntityEditing() {
        Animation anim=AnimationUtils.loadAnimation(getApplicationContext(),R.anim.translate_down_to_top);
        textEntityEditPanel.setVisibility(View.VISIBLE);
        textEntityEditPanel.startAnimation(anim);
        TextEntity textEntity = currentTextEntity();
        if (textEntity != null) {
            TextEditorDialogFragment fragment = TextEditorDialogFragment.getInstance(textEntity.getLayer().getText());
            fragment.show(getFragmentManager(), TextEditorDialogFragment.class.getName());
        }
        useTemplateRecyclerAdopter.notifyDataSetChanged();
    }

    @Nullable
    private TextEntity currentTextEntity() {
        if (motionView != null && motionView.getSelectedEntity() instanceof TextEntity) {
            return ((TextEntity) motionView.getSelectedEntity());
        } else {
            return null;
        }
    }


    protected void addTextSticker() {
        TextLayer textLayer = createTextLayer();
        TextEntity textEntity = new TextEntity(textLayer, motionView.getWidth(),
                motionView.getHeight(), fontProvider);
        motionView.addEntityAndPosition(textEntity);

        // move text sticker up so that its not hidden under keyboard
        PointF center = textEntity.absoluteCenter();
        center.y = center.y * 0.5F;
        textEntity.moveCenterTo(center);

        // redraw
        motionView.invalidate();

        startTextEntityEditing();
    }

    private TextLayer createTextLayer() {
        TextLayer textLayer = new TextLayer();
        Font font = new Font();
        font.setColor(TextLayer.Limits.INITIAL_FONT_COLOR);
        font.setSize(TextLayer.Limits.INITIAL_FONT_SIZE);
        font.setTypeface(fontProvider.getDefaultFontName());
        textLayer.setFont(font);
        if (BuildConfig.DEBUG) {
            textLayer.setText("Hello, world :))");
        }
        return textLayer;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_STICKER_REQUEST_CODE) {
                if (data != null) {
                    int stickerId = data.getIntExtra(StickerSelectActivity.EXTRA_STICKER_ID, 0);
                    if (stickerId != 0) {
                        addSticker(stickerId);
                    }
                }
            }
            if (requestCode == SELECT_VIDEO_REQUEST_CODE) {
                if (data != null) {
                    videoUri = data.getData();
                }
            }
            if (requestCode == SELECT_IMAGE_REQUEST_CODE) {
                if (data != null) {
                    imageUri = data.getData();
                    Glide.with(getApplicationContext())
                            .asBitmap()
                            .apply(new RequestOptions().override(500, 300))
                            .load(imageUri)
                            .listener(new RequestListener<Bitmap>() {
                                          @Override
                                          public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Bitmap> target, boolean b) {
                                              return false;
                                          }
                                @Override
                                public boolean onResourceReady(Bitmap bitmap, Object o, Target<Bitmap> target, DataSource dataSource, boolean b) {
                                              addImage(bitmap);
                                              return false;
                                          }
                                      }
                            ).submit();
                }
            }
        }
    }

    @Override
    public void textChanged(@NonNull String text) {
        TextEntity textEntity = currentTextEntity();
        if (textEntity != null) {
            TextLayer textLayer = textEntity.getLayer();
            if (!text.equals(textLayer.getText())) {
                textLayer.setText(text);
                textEntity.updateEntity();
                motionView.invalidate();
                useTemplateRecyclerAdopter.notifyDataSetChanged();

            }
        }
    }

    private void setUpTimeLinearContainer() {
        mHolderTopView.setMax((int) videoDuration);
        runnable=new Runnable() {
            @Override
            public void run() {
                if(isInterrupted)return;
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    int mCurrentPosition = mediaPlayer.getCurrentPosition();
                    mHolderTopView.setProgress(mCurrentPosition);
                    showElapsedTime(mCurrentPosition);
                }
                mHandler.postDelayed(this, 100);
            }
        };

        thread=new Thread(runnable);
        thread.start();
        SeekBar.OnSeekBarChangeListener timelineBarSeekBarChangeListener
                = new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser){
                    if(mediaPlayer != null){
                        mediaPlayer.seekTo(progress);
                        showElapsedTime(progress);
                        //PointF f=getSeekBarPos(mHolderTopView,linear,mTimeLineContainer);
                    }
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        };
        mHolderTopView.setOnSeekBarChangeListener(timelineBarSeekBarChangeListener);
    }

    @SuppressLint("SetTextI18n")
    private void showElapsedTime(int timeT){
        int sec=timeT/1000;
        int min=timeT/60000;
        int hour=timeT/3600000;
        min-=60*hour;
        sec-=(60*min)+(3600*hour);
        elapsedTime.setText(hour+":"+min+":"+sec);
    }

    @OnClick(R.id.choose_sticker_use_template)
    public void pickSticker(View view){
        if(!isClickable)return;
        isClickable=false;
        setupStickerPicker();
    }

    @OnClick(R.id.choose_image_use_template)
    public void pickImage(View view){
        if(!isClickable)return;
        isClickable=false;
        requestImageAccessPermission(Manifest.permission.READ_EXTERNAL_STORAGE, getString(R.string.permission_read_storage_rationale),REQUEST_STORAGE_IMAGE_ACCESS_PERMISSION);
    }

    @OnClick(R.id.choose_video_use_template)
    public void pickVideo(View view){
        videoUri = null;
        if(!isClickable)return;
        isClickable=false;
        requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, getString(R.string.permission_read_storage_rationale), REQUEST_STORAGE_READ_ACCESS_PERMISSION);
    }

    private void requestPermission(final String permission, String rationale, final int requestCode) {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getString(R.string.permission_title_rationale));
                builder.setMessage(rationale);
                builder.setPositiveButton(getString(R.string.Ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(UseYourTemplate.this, new String[]{permission}, requestCode);
                    }
                });
                builder.setNegativeButton(getString(R.string.cancel), null);
                builder.show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
            }
        }
        else {
            getVideoList();
            setupVideoPickerRecycler();
        }
    }

    private void requestImageAccessPermission(final String permission,String rationale,final int requestCode){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getString(R.string.permission_title_rationale));
                builder.setMessage(rationale);
                builder.setPositiveButton(getString(R.string.Ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(UseYourTemplate.this, new String[]{permission}, requestCode);
                    }
                });
                builder.setNegativeButton(getString(R.string.cancel), null);
                builder.show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
            }
        }
        else {
            getImagesPath();
            setupImagePicker();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getVideoList();
                    setupVideoPickerRecycler();
                } else {
                    return;
                }
                return;
            }
            case REQUEST_STORAGE_IMAGE_ACCESS_PERMISSION:{
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    getImagesPath();
                    setupImagePicker();
                }
                else return;
            }
        }
    }

    private void setupRecyclerView(){
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false));
        useTemplateRecyclerAdopter=new UseTemplateRecyclerAdopter(this,motionView.getEntities());
        recyclerView.setAdapter(useTemplateRecyclerAdopter);
    }

    @Override
    public void OnItemClick(int pos) {
        if (!isClickable)return;
        motionView.setSelectedEntity(motionView.getEntities().get(pos),true);
    }

    @OnClick(R.id.del_sticker_use_template)
    public void delSticker(){
        if(motionView.getEntities().size()>0) {
            for (int i = 0; i < motionView.getEntities().size(); i++) {
                if (motionView.getSelectedEntity()!=null) {
                    motionView.deletedSelectedEntity();
                    useTemplateRecyclerAdopter.notifyDataSetChanged();
                }
            }
        }
    }

    @OnClick(R.id.play_button_use_template)
    public void playVideo(){
        if (!isClickable)return;
        mediaPlayer.start();
        Animation anim= AnimationUtils.loadAnimation(this,R.anim.fade_in);
        Animation anim1=AnimationUtils.loadAnimation(this,R.anim.fade_out);
        playBtn.setAnimation(anim1);
        playBtn.setVisibility(View.GONE);
        pauseBtn.setAnimation(anim);
        pauseBtn.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.pause_button_use_template)
    public void pauseVideo(){
        if(!isClickable)return;
        mediaPlayer.pause();
        Animation anim= AnimationUtils.loadAnimation(this,R.anim.fade_in);
        Animation anim1=AnimationUtils.loadAnimation(this,R.anim.fade_out);
        playBtn.setAnimation(anim);
        playBtn.setVisibility(View.VISIBLE);
        pauseBtn.setAnimation(anim1);
        pauseBtn.setVisibility(View.GONE);
    }

    private void getVideoList() {
        allVideo.clear();
        Uri uri;
        Cursor cursor;
        int column_index_data,thum;
        String absolutePathOfImage = null;
        uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.Video.Media.BUCKET_DISPLAY_NAME,MediaStore.Video.Media._ID,MediaStore.Video.Thumbnails.DATA};
        final String orderBy = MediaStore.Images.Media.DATE_TAKEN;
        cursor = this.getContentResolver().query(uri, projection, null, null, orderBy + " DESC");
        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        thum = cursor.getColumnIndexOrThrow(MediaStore.Video.Thumbnails.DATA);
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);
            ModelVideo objModel = new ModelVideo();
            objModel.setBoolean_selected(false);
            objModel.setStr_path(absolutePathOfImage);
            objModel.setStr_thumb(cursor.getString(thum));
            allVideo.add(objModel);
        }
    }

    private void setupVideoPickerRecycler(){
        showVideoHolderRecyclerAdopterUseTemplate=new ShowVideoHolderRecyclerAdopterUseTemplate(getApplicationContext(),
                allVideo,this);
        videoPicker.setLayoutManager(new GridLayoutManager(getApplicationContext(),4));
        videoPicker.setAdapter(showVideoHolderRecyclerAdopterUseTemplate);
        videoPickerHolder.setVisibility(View.VISIBLE);
        Animation anim=AnimationUtils.loadAnimation(getApplicationContext(),R.anim.translate_down_to_top);
        videoPickerHolder.startAnimation(anim);
    }


    @Override
    public void onVideoClick(int pos){
        isClickable=true;
        videoUri=Uri.parse(allVideo.get(pos).getStr_path());
        try {
            mediaPlayer.reset();
            mediaPlayer.setDataSource(this.getApplicationContext(), videoUri);
            mediaPlayer.prepare();
            mediaPlayer.setDisplay(surfaceHolder);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            videoDuration=mediaPlayer.getDuration();
            setUpTimeLinearContainer();

        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer.seekTo(0);
        mediaPlayer.start();
        Animation anim= AnimationUtils.loadAnimation(this,R.anim.fade_in);
        Animation anim1=AnimationUtils.loadAnimation(this,R.anim.fade_out);
        playBtn.setAnimation(anim1);
        playBtn.setVisibility(View.GONE);
        pauseBtn.setAnimation(anim);
        pauseBtn.setVisibility(View.VISIBLE);
        Animation anim2=AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_down);
        videoPickerHolder.startAnimation(anim2);
        videoPickerHolder.setVisibility(View.GONE);
    }

    private void getImagesPath() {
        listOfAllImages.clear();
        Uri uri;
        Cursor cursor;
        int column_index_data;
        String PathOfImage;
        uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = { MediaStore.MediaColumns.DATA};
        cursor = getApplicationContext().getContentResolver().query(uri, projection, null,
                null, null);
        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        while (cursor.moveToNext()) {
            PathOfImage = cursor.getString(column_index_data);
            listOfAllImages.add(PathOfImage);
        }
    }

    private void setupImagePicker(){
        showImageHolderRecyclerAdopterUseTemplate=new ShowImageHolderRecyclerAdopterUseTemplate(this
                ,listOfAllImages,this);
        imagePicker.setLayoutManager(new GridLayoutManager(getApplicationContext(),4));
        imagePicker.setAdapter(showImageHolderRecyclerAdopterUseTemplate);
        imagePickerHolder.setVisibility(View.VISIBLE);
        Animation anim=AnimationUtils.loadAnimation(getApplicationContext(),R.anim.translate_down_to_top);
        imagePickerHolder.startAnimation(anim);
    }

    private void setupStickerPicker(){
        ArrayList<Integer> stickers=new ArrayList<>();
        for(int i=0;i<stickerIds.length;i++)stickers.add(stickerIds[i]);
        ShowStickerHolderRecyclerAdopterUseTemplate showStickerHolderRecyclerAdopterUseTemplate;
        showStickerHolderRecyclerAdopterUseTemplate=new ShowStickerHolderRecyclerAdopterUseTemplate(this,this,stickers);
        stickerPicker.setLayoutManager(new GridLayoutManager(getApplicationContext(),4));
        stickerPicker.setAdapter(showStickerHolderRecyclerAdopterUseTemplate);
        stickerPickerHolder.setVisibility(View.VISIBLE);
        Animation anim=AnimationUtils.loadAnimation(getApplicationContext(),R.anim.translate_down_to_top);
        stickerPickerHolder.startAnimation(anim);
    }

    @Override
    public void onImageClick(int pos) {
        isClickable=true;
        Glide.with(getApplicationContext())
                .asBitmap()
                .apply(new RequestOptions().override(500, 300))
                .load(listOfAllImages.get(pos))
                .listener(new RequestListener<Bitmap>() {
                              @Override
                              public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Bitmap> target, boolean b) {
                                  return false;
                              }
                              @Override
                              public boolean onResourceReady(Bitmap bitmap, Object o, Target<Bitmap> target, DataSource dataSource, boolean b) {
                                  addImage(bitmap);
                                  return false;
                              }
                          }
                ).submit();
        Animation anim=AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_down);
        imagePickerHolder.startAnimation(anim);
        imagePickerHolder.setVisibility(View.GONE);
    }

    @OnClick(R.id.add_text_use_template)
    public void addTextEntity(){
        addTextSticker();
    }

    @Override
    public void onStickerClick(int pos) {
        isClickable=true;
        addSticker(stickerIds[pos]);
        Animation anim=AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_down);
        stickerPickerHolder.startAnimation(anim);
        stickerPickerHolder.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        isClickable=true;
        Animation anim=AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_down);
        if(videoPickerHolder.getVisibility()==View.GONE&&imagePickerHolder.getVisibility()==View.GONE
                &&stickerPickerHolder.getVisibility()==View.GONE){
            finish();
        }
        if(stickerPickerHolder.getVisibility()==View.VISIBLE){
            stickerPickerHolder.startAnimation(anim);
            stickerPickerHolder.setVisibility(View.GONE);
        }
        if(imagePickerHolder.getVisibility()==View.VISIBLE){
            imagePickerHolder.startAnimation(anim);
            imagePickerHolder.setVisibility(View.GONE);
        }
        if(videoPickerHolder.getVisibility()==View.VISIBLE){
            videoPickerHolder.startAnimation(anim);
            videoPickerHolder.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.mute_video)
    public void muteVideo(){
        mediaPlayer.setVolume(0,0);
        Animation anim= AnimationUtils.loadAnimation(this,R.anim.fade_in);
        Animation anim1=AnimationUtils.loadAnimation(this,R.anim.fade_out);
        muteBtn.startAnimation(anim1);
        muteBtn.setVisibility(View.GONE);
        unmuteBtn.setVisibility(View.VISIBLE);
        unmuteBtn.startAnimation(anim);
    }

    @OnClick(R.id.unmute_video)
    public void unMuteVideo(){
        mediaPlayer.setVolume(1,1);
        Animation anim= AnimationUtils.loadAnimation(this,R.anim.fade_in);
        Animation anim1=AnimationUtils.loadAnimation(this,R.anim.fade_out);
        unmuteBtn.startAnimation(anim1);
        unmuteBtn.setVisibility(View.GONE);
        muteBtn.setVisibility(View.VISIBLE);
        muteBtn.startAnimation(anim);
    }

    @OnClick(R.id.save_video_use_your_template)
    public void saveVideo(){
        Intent intent=new Intent(UseYourTemplate.this,SaveVideoActivity.class);
        startActivity(intent);
    }

}

