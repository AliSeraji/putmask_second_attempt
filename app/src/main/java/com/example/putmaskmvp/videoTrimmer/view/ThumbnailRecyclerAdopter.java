package com.example.putmaskmvp.videoTrimmer.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.util.LongSparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.putmaskmvp.R;

import java.lang.ref.WeakReference;


public class ThumbnailRecyclerAdopter extends RecyclerView.Adapter<ThumbnailRecyclerAdopter.ThumbnailHolder>  {

    private Context mContext;
    private LongSparseArray<Bitmap> mBitmapList = null;
    private int numOfThumbs;
    private long duration;
    private Uri uri;
    private int horizontalWidth;
    public  ThumbnailRecyclerAdopter(Context context,int numOfThumbs,Uri uri,long duration){
        this.mContext=context;
        this.numOfThumbs=numOfThumbs;
        this.uri=uri;
        this.duration=duration;
        this.horizontalWidth=0;
    }

    @Override
    public ThumbnailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from (parent.getContext()).inflate(R.layout.item_thumbnail_recycler_adopter,parent,false);

        return new ThumbnailHolder(view);
    }

    @Override
    public void onBindViewHolder(final ThumbnailHolder holder, int position) {
        Log.i("test","calling");
        //holder.thumbImage.setImageResource(R.drawable.bellsprout);
        String TAG=String.valueOf(position);
        holder.thumbImage.setTag(TAG);
        MyAsyncTask1 task=new MyAsyncTask1(holder,ThumbnailRecyclerAdopter.this,uri,TAG);
        task.execute(position);

    }

    @Override
    public int getItemCount() {
        return numOfThumbs;
    }


    public static class ThumbnailHolder extends RecyclerView.ViewHolder{
        ImageView thumbImage;
        public ThumbnailHolder(View itemView) {
            super(itemView);
            thumbImage=itemView.findViewById(R.id.thumb_place);
        }
    }
    private static class MyAsyncTask1 extends AsyncTask<Integer, Void, Bitmap> {
        private WeakReference<ThumbnailRecyclerAdopter> ref;
        private WeakReference<ThumbnailHolder> holderRef;
        private String TAG;
        private Uri uri;
        private long videoLengthInMs;
        private int numThumbs;
        private long interval;
        private MediaMetadataRetriever mediaMetadataRetriever;

        public MyAsyncTask1(ThumbnailHolder holder,ThumbnailRecyclerAdopter adopter, Uri uri,String TAG){
            this.uri=uri;
            this.TAG=TAG;
            ref=new WeakReference<>(adopter);
            holderRef=new WeakReference<>(holder);
        }

        @Override
        protected Bitmap doInBackground(Integer...params) {
            ThumbnailRecyclerAdopter adopter=ref.get();
            if(adopter==null) return null;
            mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(adopter.mContext, uri);
            videoLengthInMs = Integer.parseInt(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)) * 1000;
            numThumbs=(int)adopter.duration/1000;
            interval = videoLengthInMs / numThumbs;
            Bitmap bitmap=mediaMetadataRetriever.getFrameAtTime(params[0] * interval, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap){
            ThumbnailHolder holder=holderRef.get();
            if(holder==null) return;
            if(holder.thumbImage.getTag().toString().equals(TAG)){
                holder.thumbImage.setImageBitmap(bitmap);
            }
        }
    }


}
