package com.example.putmaskmvp.videoTrimmer.view;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

public class HorizontalSpaceItemmDecoration extends RecyclerView.ItemDecoration  {


        private  int horizontalWidth;
        public HorizontalSpaceItemmDecoration(int horizontalWidth) {
            this.horizontalWidth= horizontalWidth;
        }
        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            if (parent.getChildAdapterPosition(view) != parent.getAdapter().getItemCount() - 1) {
                outRect.right = horizontalWidth;
            }
        }

}
