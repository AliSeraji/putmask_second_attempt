package com.example.putmaskmvp.videoTrimmer.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.LongSparseArray;
import android.widget.ImageView;

public  class MyAsyncTask extends AsyncTask<Integer, Void, Bitmap> {

    private ImageView imageView;
    private String TAG;
    private Uri uri;
    private Context mContext;
    private long videoLengthInMs;
    private int numThumbs;
    private long interval;
    private LongSparseArray<Bitmap> mBitmap=null;
    private MediaMetadataRetriever mediaMetadataRetriever;

    public MyAsyncTask(ImageView imageView, Uri uri,String TAG){
        this.imageView=imageView;
        this.uri=uri;
        this.TAG=TAG;
    }

    @Override
    protected Bitmap doInBackground(Integer...params) {
        mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(mContext, uri);
        videoLengthInMs = Integer.parseInt(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)) * 1000;
        numThumbs=(int)videoLengthInMs /1000000;
        interval = videoLengthInMs / numThumbs;
        Bitmap bitmap=mediaMetadataRetriever.getFrameAtTime(params[0] * interval, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap){
        if(imageView.getTag().toString().equals(TAG)){
            imageView.setImageBitmap(bitmap);
        }
    }
}
