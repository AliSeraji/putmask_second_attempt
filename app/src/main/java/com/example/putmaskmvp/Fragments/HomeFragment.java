package com.example.putmaskmvp.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.baoyz.widget.PullRefreshLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.example.putmaskmvp.Adopters.HomeRecyclerAdopter;
import com.example.putmaskmvp.Adopters.VideoPlayerRecyclerView;
import com.example.putmaskmvp.R;
import com.example.putmaskmvp.Utils.MediaObject;
import com.example.putmaskmvp.Utils.Resources;
import com.example.putmaskmvp.Utils.VerticalSpacingItemDecorator;

import java.util.ArrayList;
import java.util.Arrays;


public class HomeFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private VideoPlayerRecyclerView recyclerView;
    private OnFragmentInteractionListener mListener;
    private PullRefreshLayout pullRefreshLayout;
    private HomeRecyclerAdopter homeRecyclerAdopter;
    private long videoPlayerPos;
    public HomeFragment() {
        // Required empty public constructor
    }


    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView=view.findViewById(R.id.home_page_recycler);
        pullRefreshLayout=view.findViewById(R.id.swipeRefreshLayout);
        setupPulltoRefresh();
        setUpRecyclerAdopter();
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }
    }



    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void setUpRecyclerAdopter(){
        VerticalSpacingItemDecorator itemDecorator = new VerticalSpacingItemDecorator(10);
        ArrayList<MediaObject> mediaObjects = new ArrayList<>(Arrays.asList(Resources.MEDIA_OBJECTS));
        recyclerView.addItemDecoration(itemDecorator);
        homeRecyclerAdopter=new HomeRecyclerAdopter(mediaObjects,getContext(),initGlide());
        recyclerView.setMediaObjects(mediaObjects);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false));
        recyclerView.setAdapter(homeRecyclerAdopter);
    }

    private RequestManager initGlide(){
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.white_background)
                .error(R.drawable.white_background);
        return Glide.with(this)
                .setDefaultRequestOptions(options);
    }

    private void setupPulltoRefresh(){
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                CountDownTimer cDown=new CountDownTimer(3000,1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }
                    @Override
                    public void onFinish() {
                        pullRefreshLayout.setRefreshing(false);

                    }
                }.start();
            }
        });
    }
    @Override
    public void onPause(){
        super.onPause();
            recyclerView.pausePlayer();
            videoPlayerPos = recyclerView.getVideoPlayerCurrentPos();

    }

    @Override
    public void onResume(){
        super.onResume();
            recyclerView.setVideoPlayerCurrentPos(videoPlayerPos);

    }

    public void onPauseFragment() {
        recyclerView.pausePlayer();
        videoPlayerPos=recyclerView.getVideoPlayerCurrentPos();

    }

    public void onResumeFragment() {
        recyclerView.setVideoPlayerCurrentPos(videoPlayerPos);
    }


}
