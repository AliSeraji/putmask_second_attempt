package com.example.putmaskmvp.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.example.putmaskmvp.Adopters.ProfilePagerAdopter;
import com.example.putmaskmvp.R;
import com.gauravk.bubblenavigation.BubbleNavigationConstraintView;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.jackandphantom.circularimageview.CircleImage;

import static android.app.Activity.RESULT_OK;


public class ProfileFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private CircleImage editProfile;
    private CircleImage userImage;
    private TextView userName,status;
    private TextInputEditText editUserName,editStatus;
    private String userNameText,statusText;
    private Button saveChanges,cancelChanges;
    private TextInputLayout statusLayout,userNameLayout;
    private boolean isEditMode=false;
    private final int SELECT_IMAGE_REQUEST_CODE = 11;
    private BubbleNavigationConstraintView bubbleNavigationConstraintView;
    private ViewPager viewPager;
    private ProfilePagerAdopter profilePagerAdopter;
    private Uri imageUri;
    private com.google.android.material.tabs.TabLayout tableLayout;
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ProfileFragment() {

    }
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_profile, container, false);
        findIds(view);
        setUpListeners();
        setUpViewPager();
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else { }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void findIds(View view){
        tableLayout=view.findViewById(R.id.profile_pager);
        editProfile=view.findViewById(R.id.edit_profile);
        editStatus=view.findViewById(R.id.edit_user_status);
        editUserName=view.findViewById(R.id.edit_user_name);
        userImage=view.findViewById(R.id.profile_pic);
        userName=view.findViewById(R.id.userName);
        status=view.findViewById(R.id.user_status);
        saveChanges=view.findViewById(R.id.saveChanges);
        cancelChanges=view.findViewById(R.id.cancelChanges);
        statusLayout=view.findViewById(R.id.status_layout);
        userNameLayout=view.findViewById(R.id.userName_layout);
        //bubbleNavigationConstraintView=view.findViewById(R.id.profile_pager);
        viewPager=view.findViewById(R.id.profile_view_pager1);
    }

    private void setUpListeners(){
        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEditMode=true;
                statusLayout.setVisibility(View.VISIBLE);
                userNameLayout.setVisibility(View.VISIBLE);
                editStatus.setVisibility(View.VISIBLE);
                editUserName.setVisibility(View.VISIBLE);
                saveChanges.setVisibility(View.VISIBLE);
                cancelChanges.setVisibility(View.VISIBLE);
                userNameText=userName.getText().toString();
                statusText=status.getText().toString();
                userName.setVisibility(View.GONE);
                status.setVisibility(View.GONE);
                editStatus.setText(statusText);
                editUserName.setText(userNameText);

            }
        });
        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isEditMode){
                    imageUri = null;
                    Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickIntent.createChooser(pickIntent, "select image"), SELECT_IMAGE_REQUEST_CODE);

                }
                else return;
            }
        });
        saveChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editStatus.getText().toString().length()>0 && editUserName.getText().toString().length()>0) {
                    statusText = editStatus.getText().toString();
                    userNameText = editUserName.getText().toString();
                    userName.setText(userNameText);
                    status.setText(statusText);
                    editStatus.setVisibility(View.GONE);
                    editUserName.setVisibility(View.GONE);
                    userName.setVisibility(View.VISIBLE);
                    status.setVisibility(View.VISIBLE);
                    saveChanges.setVisibility(View.GONE);
                    cancelChanges.setVisibility(View.GONE);
                    statusLayout.setVisibility(View.GONE);
                    userNameLayout.setVisibility(View.GONE);
                    isEditMode=false;
                }
            }
        });
        cancelChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editStatus.setVisibility(View.GONE);
                editUserName.setVisibility(View.GONE);
                userName.setVisibility(View.VISIBLE);
                status.setVisibility(View.VISIBLE);
                saveChanges.setVisibility(View.GONE);
                cancelChanges.setVisibility(View.GONE);
                statusLayout.setVisibility(View.GONE);
                userNameLayout.setVisibility(View.GONE);
                isEditMode=false;
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_IMAGE_REQUEST_CODE) {
                imageUri = data.getData();
                Glide.with(getActivity().getApplicationContext())
                        .load(imageUri).fitCenter()
                        .override(500, 758)
                        .into(userImage);
            }
        }
    }

    private void setUpViewPager(){



        profilePagerAdopter=new ProfilePagerAdopter(getFragmentManager());
        viewPager.setAdapter(profilePagerAdopter);



        tableLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition(),true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        /*bubbleNavigationConstraintView.setNavigationChangeListener(new BubbleNavigationChangeListener() {
            @Override
            public void onNavigationChanged(View view, int position) {
                viewPager.setCurrentItem(position,true);
            }
        });*/
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }
            @Override
            public void onPageSelected(int i) {
                //bubbleNavigationConstraintView.setCurrentActiveItem(i);
                //TabLayout.Tab tab=tableLayout.getTabAt(i);
                //tableLayout.getTabAt(i).select();
                //tab.select();
                tableLayout.setScrollPosition(i,0f,true);

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }





}
