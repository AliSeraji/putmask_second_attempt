package com.example.putmaskmvp.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.example.putmaskmvp.Adopters.SearchRecyclerAdopter;
import com.example.putmaskmvp.R;

import java.util.ArrayList;


public class SearchFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private FloatingSearchView floatingSearchView;
    private ArrayList<String> generatedData=new ArrayList<>();
    private ArrayList<String> result=new ArrayList<>();
    private RecyclerView recyclerView;
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private SearchRecyclerAdopter searchRecyclerAdopter;


    public SearchFragment() {
        // Required empty public constructor
    }

    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_search, container, false);
        generatedData.clear();
        generateFakeData();
        floatingSearchView=view.findViewById(R.id.floating_search_view);
        recyclerView=view.findViewById(R.id.search_recycler);
        onSearch();
        prepareRecyclerView();
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }

    private void onSearch(){
        floatingSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, final String newQuery) {

                if(newQuery.length()>0) {
                    searchRecyclerAdopter.notifyDataSetChanged();
                    result.clear();
                    search(newQuery);
                }
                else{
                    result.clear();
                }
                searchRecyclerAdopter.notifyDataSetChanged();
                //get suggestions based on newQuery

                //pass them on to the search view
                //floatingSearchView.swapSuggestions();
            }
        });

    }

    private void search(String str){
        for(int i=0;i<generatedData.size();i++){
            if(generatedData.get(i).contains(str)){
                result.add(generatedData.get(i));
            }
        }
    }
    private void generateFakeData(){

        generatedData.add("Ali seraji");
        generatedData.add("Farhad Kiani");
        generatedData.add("Soheil Vafaian");
        generatedData.add("Amin kazemi");
        generatedData.add("Ali Safari");
        generatedData.add("Mostafa Ebrahimi");
        generatedData.add("Mohamad Hossein Shojainia");
        for (int i=0;i<50;i++){
            generatedData.add("UserName "+i);
        }
        for(int i=50;i<100;i++){
            generatedData.add("pageName "+i);
        }
    }
    private void prepareRecyclerView(){
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        searchRecyclerAdopter=new SearchRecyclerAdopter(result);
        recyclerView.setAdapter(searchRecyclerAdopter);
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

}
