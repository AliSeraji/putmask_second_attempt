package com.example.putmaskmvp.Fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.putmaskmvp.Adopters.ProfileRecyclerAdopter;
import com.example.putmaskmvp.R;

import io.alterac.blurkit.BlurKit;
import io.alterac.blurkit.BlurLayout;


public class PostedFragment extends Fragment implements ProfileRecyclerAdopter.OnProfileRecyclerItemListener{
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private RecyclerView recyclerView;
    private ProfileRecyclerAdopter profileRecyclerAdopter;
    private String mParam1;
    private String mParam2;
    private View post;
    private View v;
    private View layout;
    private BlurLayout blurView;
    private Button close;
    private Dialog dialog;
    private float movement = 150;

    private OnFragmentInteractionListener mListener;

    public PostedFragment() {
        // Required empty public constructor
    }

    public static PostedFragment newInstance(String param1, String param2) {
        PostedFragment fragment = new PostedFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        BlurKit.init(this.getContext());
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_posted, container, false);
        findViews(view);
        setUpRecyclerview();
        dialog=new Dialog(getContext());
        dialog.setContentView(R.layout.view_own_post);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation anim= AnimationUtils.loadAnimation(getContext(),R.anim.fade_out);
                //dialog.dismiss();
                post.setAnimation(anim);
                post.setVisibility(View.GONE);
            }
        });
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void OnItemClick(int position) {
        Animation anim=AnimationUtils.loadAnimation(getContext(),R.anim.fade_in);
        post.setAnimation(anim);
        post.setVisibility(View.VISIBLE);
        post.setElevation(10);
        //dialog.show();
        BlurKit.getInstance().fastBlur(recyclerView, 12,.25f);
        //startBlur();

    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void findViews(View view){
        recyclerView=view.findViewById(R.id.posted_posts_recycler);
        post=view.findViewById(R.id.show_own_post);
        close=view.findViewById(R.id.close_view_own);
        blurView=view.findViewById(R.id.blurLayout);
        layout=view.findViewById(R.id.relativeLayout);
    }


    private void setUpRecyclerview(){
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),3));
        profileRecyclerAdopter=new ProfileRecyclerAdopter(getActivity(),25,this);
        recyclerView.setAdapter(profileRecyclerAdopter);
    }


    @Override
    public void onStart(){
        super.onStart();
        //blurView.startBlur();
        //blurView.lockView();
    }

    @Override
    public void onStop(){
        super.onStop();
        //blurView.pauseBlur();
    }

    private void startBlur(){
        blurView.animate().translationY(movement).setDuration(1500).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                movement = movement > 0 ? -150 : 150;
                blurView.animate().translationY(movement).setDuration(1500).setListener(this).start();
            }
        }).start();

    }

}
