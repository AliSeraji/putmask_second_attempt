package com.example.putmaskmvp.Fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.putmaskmvp.Adopters.ModelVideo;
import com.example.putmaskmvp.Adopters.ShareRecyclerAdopter;
import com.example.putmaskmvp.R;
import com.example.putmaskmvp.TrimmerActivity;
import com.example.putmaskmvp.videoTrimmer.utils.FileUtils;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.File;
import java.util.ArrayList;


public class ShareFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    private ArrayList<String> pathArrList=new ArrayList<>();
    private ArrayList<ModelVideo> allVideo = new ArrayList<>();
    private RecyclerView recyclerView;
    private PlayerView videoView;
    private ImageView imageView;
    private Button button;
    static final String EXTRA_VIDEO_PATH = "EXTRA_VIDEO_PATH";
    static final String VIDEO_TOTAL_DURATION = "VIDEO_TOTAL_DURATION";
    private ShareRecyclerAdopter shareRecyclerAdopter;
    private SimpleExoPlayer player;
    private RelativeLayout videoPlayerHolder;
    private MKLoader videoRecyclerLoaderBar;
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ShareFragment() {

    }

    public static ShareFragment newInstance(String param1,String param2) {
        ShareFragment fragment = new ShareFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_share, container, false);
        recyclerView=view.findViewById(R.id.share_recycler_holder);
        videoView=view.findViewById(R.id.selected_video);
        videoPlayerHolder=view.findViewById(R.id.up_1);
        imageView=view.findViewById(R.id.play_video_in_share_fragment);
        videoRecyclerLoaderBar=view.findViewById(R.id.video_recycler_loader_bar);
        button=view.findViewById(R.id.next_activity);
        setUpPlayer();
        setUpRecyclerView();
        playVideo();
        pauseVideo();
        playerListener();
        gotoNextActivity();
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void getVideoList() {
        allVideo.clear();
        Uri uri;
        Cursor cursor;
        int column_index_data,thum;

        String absolutePathOfImage = null;
        uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.Video.Media.BUCKET_DISPLAY_NAME,MediaStore.Video.Media._ID,MediaStore.Video.Thumbnails.DATA};

        final String orderBy = MediaStore.Images.Media.DATE_TAKEN;
        cursor = getContext().getContentResolver().query(uri, projection, null, null, orderBy + " DESC");

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        thum = cursor.getColumnIndexOrThrow(MediaStore.Video.Thumbnails.DATA);


        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);

            ModelVideo objModel = new ModelVideo();
            objModel.setBoolean_selected(false);
            objModel.setStr_path(absolutePathOfImage);
            objModel.setStr_thumb(cursor.getString(thum));
            allVideo.add(objModel);
        }
        videoRecyclerLoaderBar.setVisibility(View.GONE);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,@NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getVideoList();

                } else {
                    return;
                }
                return;
            }
        }
    }

    private void requestPermission(final String permission, String rationale, final int requestCode) {

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            //if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) getContext(), permission)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getString(R.string.permission_title_rationale));
                builder.setMessage(rationale);
                builder.setPositiveButton(getString(R.string.Ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ActivityCompat.requestPermissions(getActivity(), new String[]{permission}, requestCode);

                    }
                });
                builder.setNegativeButton(getString(R.string.cancel), null);
                builder.show();



            //} else {
              //  ActivityCompat.requestPermissions((Activity)getContext(), new String[]{permission}, requestCode);
                //getVideoList();
            //}
        }
        else {
            getVideoList();
        }
    }

    private void setUpRecyclerView(){
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),4));
        shareRecyclerAdopter=new ShareRecyclerAdopter(getContext(),allVideo,player,imageView);
        recyclerView.setAdapter(shareRecyclerAdopter);
    }

    private void playVideo(){
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isPlaying()) {
                    player.seekTo(0);
                    player.setPlayWhenReady(true);
                    player.getPlaybackState();
                    Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
                    imageView.startAnimation(anim);
                    imageView.setVisibility(View.GONE);
                }
            }
        });
    }
    private void pauseVideo(){
        videoPlayerHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPlaying()){
                    player.setPlayWhenReady(false);
                    player.getPlaybackState();
                    Animation anim = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
                    imageView.startAnimation(anim);
                    imageView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void gotoNextActivity(){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(shareRecyclerAdopter.getPath()!=null) {
                    Intent intent = new Intent(getContext(), TrimmerActivity.class);
                    intent.putExtra(EXTRA_VIDEO_PATH, FileUtils.getPath(getContext(), Uri.fromFile(new File(shareRecyclerAdopter.getPath()))));
                    intent.putExtra(VIDEO_TOTAL_DURATION, player.getDuration());
                    getContext().startActivity(intent);
                }
            }
        });
    }

    private void setUpPlayer() {
        BandwidthMeter bandwidthMeter= new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);
        player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(getContext()), trackSelector);
        videoView.setUseController(false);
        videoView.setPlayer(player);
    }

    private boolean isPlaying() {
        return player.getPlaybackState() == Player.STATE_READY && player.getPlayWhenReady();
    }

    @Override
    public void onPause(){
        super.onPause();
        player.setPlayWhenReady(false);
        player.getPlaybackState();

    }

    @Override
    public void onResume(){
        super.onResume();

        imageView.setVisibility(View.GONE);
        player.seekTo(0);
        player.setPlayWhenReady(true);
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser && getActivity()!=null) {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, getString(R.string.permission_read_storage_rationale)
                    , REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        }
    }

    private void playerListener(){
        player.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState){
                    case Player.STATE_ENDED:
                        player.seekTo(0);
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });
    }

}
