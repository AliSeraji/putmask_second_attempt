package com.example.putmaskmvp;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.putmaskmvp.Adopters.ViewProfileRecyclerAdopter;

public class ViewOthersProfile extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ViewProfileRecyclerAdopter viewProfileRecyclerAdopter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_others_profile);
        recyclerView=findViewById(R.id.view_oprofile_recycler_adopter);
        setUpRecyclerview();
    }

    private void setUpRecyclerview(){
        recyclerView.setLayoutManager(new GridLayoutManager(this,3));
        viewProfileRecyclerAdopter=new ViewProfileRecyclerAdopter(200);
        recyclerView.setAdapter(viewProfileRecyclerAdopter);
    }
}
