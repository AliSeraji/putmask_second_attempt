package com.example.putmaskmvp;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.putmaskmvp.Adopters.CommentRecyclerAdopter;

public class CommentActivity extends AppCompatActivity {


    private RecyclerView recyclerView;
    private CommentRecyclerAdopter commentRecyclerAdopter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        recyclerView=findViewById(R.id.comment_holder);
        setUpRecyclerview();
    }
    private void setUpRecyclerview(){
        recyclerView.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        commentRecyclerAdopter=new CommentRecyclerAdopter(1000);
        recyclerView.setAdapter(commentRecyclerAdopter);

    }

}
